# Welcome to VTT BA1619's Kubernetes Demonstration

This demonstration was developed originally for my KubeCon EU 2019 talk,
and later modified to be used for internal training and demonstrations. 

It consists of an API definition, using [gRPC](https://grpc.io/), Java 
based servers and JavaScript / Java based clients. It also has a 
[Pulumi](https://www.pulumi.com/) based provisioning system for deploying
the server-side components into Kubernetes clusters.  

## Project layout

    api-definition/    # The gRPC definitions
    documentation/     # The source for the documentation site
    gitops/            # Kubernetes deployment YAMLs
    java/              # Java components (servers, clients)
    provision/         # Pulumi based provisioning 
    web-client/        # Web client (Vue)
    LICENSE            # License file (Apache 2.0)
    

