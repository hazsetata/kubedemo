# Automated provisioning

This project sets up the pipeline components on a Kubernetes cluster. To be able to use it, 
please follow the steps bellow.

## Prerequisites

This provisioning project uses [Pulumi](https://pulumi.io/) and [Helm 2](https://helm.sh/) 
to create and configure a Kubernetes cluster with all the necessary components. The
next paragraphs will describe what tools / tool configurations are
required for this project to function correctly.

### Create Git access key

If you want to utilize the GitOps tools that can be optionally installed by the provisioning
project, you will need to have create an SSH Key that [Flux](https://docs.fluxcd.io/en/latest/index.html),
the GitOps tool we support can use. A new key can be created like this:

```bash
ssh-keygen -t ed25519 -f git-access -N ""
```

and then converted into a Kubernetes secret like this:

```bash
kubectl create secret generic flux-git-deploy -n flux --from-file=identity=./git-access --dry-run -o yaml > ./git-access.key.yaml
```

Important: the key **must** be named `flux-git-deploy` and it **must** be generated for the 
namespace `flux`.

Once the key-pair is ready, the public key must be registered with the Git provider.

### Install and configure Pulumi

Please follow the install instructions on the [Pulumi installation](https://pulumi.io/install/)
page.

### Install and configure Helm

Several components of this installation uses official Helm charts, and this functionality
requires the Helm client to be installed. Helm binaries can be found on the project's
[GitHub page](https://github.com/helm/helm). If you are not using a platform-installer
package, please place the binary in a directory on your PATH.

!!! warning
    At the moment Pulumi's Kubernetes provider supports Helm version 2 **only**.
    Please make sure that the command below reports client version 2.x.x.

    ```console
    helm version
    ```

We will use Helm in a client-only mode (the server-side Tiller component won't be used), so
we have to configure it accordingly:

```console
helm init --client-only
```

The installation scripts use some Helm charts that are not in the default Helm chart 
repository, these repositories have to be added:

Jetstack's [Cert-Manager](https://docs.cert-manager.io/en/release-0.7/index.html): 

```console
helm repo add jetstack https://charts.jetstack.io
```

Once all the repositories are configured, we should update the repository information:

```console
helm repo update
```

### Configure project values

There are some configuration options defined for this project. Most of 
them has sensible defaults, but the security related options should be 
set by you. The full list of available options:

Option name | Default value | Secret | Description
------------|---------------|:------:|------------
name|VTTResearch|no|Base name for deployment
kubernetesType|digitalocean|no|Name of the Kubernetes provider to be used (only **digitalocean** is supported for this demo)
installIngress|false|no|Install an ingress-controller
ingressType|ambassador|no|The ingress controller to be deployed (only **ambassador** is supported for this demo)
dnsType|cloudflare|no|The DNS provider to be used for registering the ingress's public IP. Also used for LetsEncrypt domain validation (only **cloudflare** is supported for this demo)
acmeEmail|*none*|no|The email to be used for the LetsEncrypt certificate creation
acmeProduction|false|no|If true LetsEncrypt's production issuer will be used (staging issuer otherwise)
installArgo|false|no|If true Argo workflow engine will be installed
installGitOpsTools|false|no|If true FluxCD and Flagger will be installed
vttcloudflaredns:email|*none*|yes|The email for the Cloudflare account that will be used for DNS operations
vttcloudflaredns:apiKey|*none*|yes|The API key for the Cloudflare account that will be used for DNS operations
vttcloudflaredns:zoneId|*none*|yes|The ID of the Cloudflare zone that will be used for DNS operations
vttcloudflaredns:dnsPostfix|*none*|no|An optional postfix that can be added between the stack name and the name the zone represents
vttcloudflaredns:createWildcard|false|no|If true a wildcard DNS entry will be **also* created
vttcloudflaredns:ttl|3600|no|The TTL for the DNS entries
vttdokubernetes:region|AMS3|no|The DigitalOcean region where the K8s cluster will be created
vttdokubernetes:kubernetesVersion|latest|no|The K8s version to be used when creating the cluster in DigitalOcean
vttdokubernetes:nodeCount|3|no|The number of K8s nodes to be in the DigitalOcean cluster
vttdokubernetes:nodeSize|s-2vcpu-4gb|no|The size of the K8s nodes in DigitalOcean
vttdokubernetes:apiToken|*none*|yes|The DigitalOcean API token
vttweaveflux:gitUrl|*none*|no|The URL for the Git repository where the deployment descriptors that FluxCD should monitor (and deploy) can be found
vttweaveflux:gitBranch|master|no|The Git branch FluxCD should monitor
vttweaveflux:gitPath|*none*|no|The (optional) path within the Git repository that FluxCD should monitor. The default is the repository root
vttweaveflux:gitIdentityKeyPath|*none*|no|The local path (on the machine where the provisioning script will be run) that points to the private key that should be used for the Git operations
vttweaveflux:gitReadOnlyAccess|false|no|If true FluxCD won't save any information to the Git repository about its operations






#### Secret values

Secret values (e.g.: passwords, API keys) should be set like this:

```
pulumi config set servicePassword --secret <YOUR_PASSWORD_HERE>
``` 

#### Standard values

Standard values can be set like this:

```
pulumi config set environment public
``` 

#### File values

File values can be set like this:

```
pulumi config set sshPublicKey < RSA_Key.pub
``` 

## Troubleshooting

### Pulumi stuck in an error state

In case the stack gets into a stuck state, where even `pulumi destroy` can't handle it,
it is possible to edit the state:

```
pulumi stack export > state.json
```

then edit this file (e.g.: manually remove the resource that Pulumi got stuck on), and then
re-import the state:

```
pulumi stack import < state.json
```
