# Documentation

## Tools
The documentation uses [MkDocs](https://www.mkdocs.org/) and the 
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) theme.

I found the easiest to work with MkDocs is to use the Docker container 
prepared by the developer of the Material for MkDocs theme. It is also
possible to install MkDocs locally - the installation process is described
in detail at the [Getting started](https://squidfunk.github.io/mkdocs-material/getting-started/) 
page of the theme or on the [MkDocs site](https://www.mkdocs.org/#installation).
  
## Live preview

MkDocs provides a live preview feature that is very useful when working
on the documentation content. To start the live preview server, run the
following from the `documentation` folder:

``` bash tab="Linux / MacOS"
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material
```

``` tab="Windows"
docker run --rm -it -p 8000:8000 -v %cd%:/docs squidfunk/mkdocs-material
``` 
 
## Build the static documentation site
Once the documentation changes are done, the static documentation site
can be built by running the following from the `documentation` folder:

``` bash tab="Linux / MacOS"
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material build
```

``` tab="Windows"
docker run --rm -it -p 8000:8000 -v %cd%:/docs squidfunk/mkdocs-material build
``` 

If besides editing the content structure was reorganized and / or a page
was deleted some stale content might be left behind. In these cases run 
the build process with the `--clean` switch. 

``` bash tab="Linux / MacOS"
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material build --clean
```

``` tab="Windows"
docker run --rm -it -p 8000:8000 -v %cd%:/docs squidfunk/mkdocs-material build --clean
``` 

The generated content will be available in the `site` folder.