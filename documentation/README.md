# Documentation

The documentation uses [MkDocs](https://www.mkdocs.org/) and the 
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) theme.

## Preview
To preview the documentation, run from this folder:

``` bash
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material
``` 
## Build the static documentation site
To build the static documentation site, run from this folder:

``` bash
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material build
```

The site content will be in the `site` folder.