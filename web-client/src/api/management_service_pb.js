// source: management_service.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var commons_pb = require('./commons_pb.js');
goog.object.extend(proto, commons_pb);
goog.exportSymbol('proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo', null, global);
goog.exportSymbol('proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest', null, global);
goog.exportSymbol('proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.displayName = 'proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.displayName = 'proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.displayName = 'proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.toObject = function(opt_includeInstance) {
  return proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.toObject = function(includeInstance, msg) {
  var f, obj = {
    buildname: jspb.Message.getFieldWithDefault(msg, 1, ""),
    buildversion: jspb.Message.getFieldWithDefault(msg, 2, ""),
    gitbranch: jspb.Message.getFieldWithDefault(msg, 3, ""),
    gitcommitid: jspb.Message.getFieldWithDefault(msg, 4, ""),
    serverinstanceid: jspb.Message.getFieldWithDefault(msg, 5, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo;
  return proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBuildname(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setBuildversion(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setGitbranch(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setGitcommitid(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setServerinstanceid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBuildname();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getBuildversion();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getGitbranch();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getGitcommitid();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getServerinstanceid();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
};


/**
 * optional string buildName = 1;
 * @return {string}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.getBuildname = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.setBuildname = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string buildVersion = 2;
 * @return {string}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.getBuildversion = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.setBuildversion = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string gitBranch = 3;
 * @return {string}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.getGitbranch = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.setGitbranch = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string gitCommitId = 4;
 * @return {string}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.getGitcommitid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.setGitcommitid = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string serverInstanceId = 5;
 * @return {string}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.getServerinstanceid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.prototype.setServerinstanceid = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest;
  return proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    serverinfo: (f = msg.getServerinfo()) && proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse;
  return proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo;
      reader.readMessage(value,proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.deserializeBinaryFromReader);
      msg.setServerinfo(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getServerinfo();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo.serializeBinaryToWriter
    );
  }
};


/**
 * optional ServerInfo serverInfo = 1;
 * @return {?proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.prototype.getServerinfo = function() {
  return /** @type{?proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo} */ (
    jspb.Message.getWrapperField(this, proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo, 1));
};


/**
 * @param {?proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfo|undefined} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse} returns this
*/
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.prototype.setServerinfo = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.prototype.clearServerinfo = function() {
  return this.setServerinfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.prototype.hasServerinfo = function() {
  return jspb.Message.getField(this, 1) != null;
};


goog.object.extend(exports, proto.fi.vtt.ba1619.kubedemo.api.v1);
