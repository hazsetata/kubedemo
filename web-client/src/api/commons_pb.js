// source: commons.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue', null, global);
goog.exportSymbol('proto.fi.vtt.ba1619.kubedemo.api.v1.Link', null, global);
goog.exportSymbol('proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue', null, global);
goog.exportSymbol('proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.displayName = 'proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.displayName = 'proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.displayName = 'proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fi.vtt.ba1619.kubedemo.api.v1.Link, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fi.vtt.ba1619.kubedemo.api.v1.Link.displayName = 'proto.fi.vtt.ba1619.kubedemo.api.v1.Link';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.prototype.toObject = function(opt_includeInstance) {
  return proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: jspb.Message.getFloatingPointFieldWithDefault(msg, 1, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue;
  return proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue();
  if (f !== 0.0) {
    writer.writeDouble(
      1,
      f
    );
  }
};


/**
 * optional double value = 1;
 * @return {number}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.prototype.getValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 1, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.DoubleValue.prototype.setValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.prototype.toObject = function(opt_includeInstance) {
  return proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue;
  return proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string value = 1;
 * @return {string}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.prototype.getValue = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.StringValue.prototype.setValue = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.prototype.toObject = function(opt_includeInstance) {
  return proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.toObject = function(includeInstance, msg) {
  var f, obj = {
    epochsecond: jspb.Message.getFieldWithDefault(msg, 1, 0),
    nanoofsecond: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp;
  return proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setEpochsecond(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNanoofsecond(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEpochsecond();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getNanoofsecond();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
};


/**
 * optional int64 epochSecond = 1;
 * @return {number}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.prototype.getEpochsecond = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.prototype.setEpochsecond = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int32 nanoOfSecond = 2;
 * @return {number}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.prototype.getNanoofsecond = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.TimeStamp.prototype.setNanoofsecond = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.prototype.toObject = function(opt_includeInstance) {
  return proto.fi.vtt.ba1619.kubedemo.api.v1.Link.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.Link} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.toObject = function(includeInstance, msg) {
  var f, obj = {
    url: jspb.Message.getFieldWithDefault(msg, 1, ""),
    name: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.Link}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fi.vtt.ba1619.kubedemo.api.v1.Link;
  return proto.fi.vtt.ba1619.kubedemo.api.v1.Link.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.Link} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.Link}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUrl(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fi.vtt.ba1619.kubedemo.api.v1.Link.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.Link} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUrl();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string url = 1;
 * @return {string}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.prototype.getUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.Link} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.prototype.setUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fi.vtt.ba1619.kubedemo.api.v1.Link} returns this
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.Link.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


goog.object.extend(exports, proto.fi.vtt.ba1619.kubedemo.api.v1);
