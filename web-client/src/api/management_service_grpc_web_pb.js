/**
 * @fileoverview gRPC-Web generated client stub for fi.vtt.ba1619.kubedemo.api.v1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var commons_pb = require('./commons_pb.js')
const proto = {};
proto.fi = {};
proto.fi.vtt = {};
proto.fi.vtt.ba1619 = {};
proto.fi.vtt.ba1619.kubedemo = {};
proto.fi.vtt.ba1619.kubedemo.api = {};
proto.fi.vtt.ba1619.kubedemo.api.v1 = require('./management_service_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ManagementClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ManagementPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest,
 *   !proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse>}
 */
const methodInfo_Management_getServerInfo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse,
  /** @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse.deserializeBinary
);


/**
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ManagementClient.prototype.getServerInfo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/fi.vtt.ba1619.kubedemo.api.v1.Management/getServerInfo',
      request,
      metadata || {},
      methodInfo_Management_getServerInfo,
      callback);
};


/**
 * @param {!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.fi.vtt.ba1619.kubedemo.api.v1.ServerInfoResponse>}
 *     A native promise that resolves to the response
 */
proto.fi.vtt.ba1619.kubedemo.api.v1.ManagementPromiseClient.prototype.getServerInfo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/fi.vtt.ba1619.kubedemo.api.v1.Management/getServerInfo',
      request,
      metadata || {},
      methodInfo_Management_getServerInfo);
};


module.exports = proto.fi.vtt.ba1619.kubedemo.api.v1;

