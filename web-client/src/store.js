import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    ui: {
      autoUpdateInterval: 3,
      sensitiveDemo: false
    },
    serverInfo: {
      connectionTried: false,
      success: false,
      versionInfo: {
        version: null,
        gitBranch: null,
        gitCommitId: null
      }
    },
    bands: {
      entries: null
    }
  },
  getters: {
    getUIAutoUpdateInterval: state => {
      return state.ui.autoUpdateInterval;
    },
    isSensitiveDemo: state => {
      return state.ui.sensitiveDemo
    },
    hasBandEntries: state => {
      return state.bands.entries != null;
    },
    getBandEntries: state => {
      return state.bands.entries;
    },
    isServerConnectionTried: state => {
      return state.serverInfo.connectionTried;
    },
    isServerConnectionSuccessful: state => {
      return state.serverInfo.connectionTried && state.serverInfo.success;
    },
    getServerVersionInfo: state => {
      return state.serverInfo.versionInfo;
    }
  },
  mutations: {
    SET_SERVER_CONNECTION: (state, connectionInfo) => {
      state.serverInfo.connectionTried = true;
      state.serverInfo.success = connectionInfo.connectionSuccessful;
      if (connectionInfo.connectionSuccessful) {
        state.serverInfo.versionInfo.version = connectionInfo.version;
        state.serverInfo.versionInfo.gitBranch = connectionInfo.gitBranch;
        state.serverInfo.versionInfo.gitCommitId = connectionInfo.gitCommitId;
      }
    },
    RESET_SERVER_CONNECTION: (state) => {
      state.serverInfo.connectionTried = false;
      state.serverInfo.success = false;
      state.serverInfo.versionInfo.version = null;
      state.serverInfo.versionInfo.gitBranch = null;
      state.serverInfo.versionInfo.gitCommitId = null;
    },
    SET_UI_AUTO_UPDATE_INTERVAL: (state, value) => {
      state.ui.autoUpdateInterval = value;
    },
    SET_BAND_DATA: (state, value) => {
      state.bands.entries = value.list;
      state.ui.sensitiveDemo = value.sensitive;
    }
  },
  actions: {
    doSetServerConnectionInfo: (context, connectionInfo) => {
      context.commit('SET_SERVER_CONNECTION', connectionInfo);
    },
    doResetServerConnectionInfo: (context) => {
      context.commit('RESET_SERVER_CONNECTION');
    },
    doSetUiAutoUpdateInterval: (context, value) => {
      context.commit('SET_UI_AUTO_UPDATE_INTERVAL', value);
    },
    doSetBandData: (context, bandData) => {
      context.commit('SET_BAND_DATA', bandData);
    }
  }
});

export default store;