import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: colors.amber.darken4,
    secondary: colors.amber.darken3,
    accent: colors.amber.lighten1
  },
  iconfont: 'md',
})
