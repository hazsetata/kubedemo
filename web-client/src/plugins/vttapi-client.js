const { ServerInfoRequest } = require('../api/management_service_pb.js');
const { BandListRequest } = require('../api/band_provider_service_pb.js');

import { ManagementClient } from "../api/management_service_grpc_web_pb";
import { BandsClient } from "../api/band_provider_service_grpc_web_pb";

import Vue from 'vue'
import store from '../store'
import { mapActions } from 'vuex';

let managementClient = new ManagementClient(process.env.VUE_APP_VTT_API_SERVER);
let bandsClient = new BandsClient(process.env.VUE_APP_VTT_API_SERVER);

let vttapi = new Vue({
    store,
    methods: {
        ...mapActions(['doSetServerConnectionInfo']),
        getServerInfo() {
            return new Promise((resolve) => {
                let request = new ServerInfoRequest();
                managementClient.getServerInfo(request, {}, (err, response) => {
                    if (!err) {
                        let serverInfo = response.getServerinfo();

                        let connectionInfo = {
                            connectionSuccessful: true,
                            version: serverInfo.getBuildversion(),
                            gitBranch: serverInfo.getGitbranch(),
                            gitCommitId: serverInfo.getGitcommitid()
                        };

                        this.doSetServerConnectionInfo(connectionInfo);

                        console.log(connectionInfo);

                        resolve();
                    }
                    else {
                        let connectionInfo = {
                            connectionSuccessful: false
                        };

                        console.log(err);

                        this.doSetServerConnectionInfo(connectionInfo);
                        resolve();
                    }
                });
            });
        },
        getBandList() {
            return new Promise((resolve) => {
                let request = new BandListRequest();
                bandsClient.getBands(request, {}, (err, response) => {
                    if (!err) {
                        let retValue = {
                            list: response.getBandsList(),
                            sensitive: response.getSensitivedemo()
                        };
                        // let bandEntries = response.getBandsList();
                        resolve(retValue);
                    }
                    else {
                        console.log(err);

                        let retValue = {
                            list: [],
                            sensitive: false
                        };

                        resolve(retValue);
                    }
                });
            });
        }
    }
});

export default {
    install: function(Vue) {
        Vue.prototype.$vttapi = vttapi;
    }
}
