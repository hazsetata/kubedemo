# Builder step
FROM node:12.12.0-alpine as builder
WORKDIR /appbuild
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# Actual image step
FROM nginx:1.17.5-alpine as kubedemo
LABEL vendor="Teknologian tutkimuskeskus VTT Oy" maintainer="zsolt.homorodi@vtt.fi"

# Install / update Tini (simplest init for containers: https://github.com/krallin/tini)
RUN apk add --update tini

EXPOSE 80
CMD ["/sbin/tini", "--", "nginx"]

COPY --from=builder /appbuild/nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /appbuild/dist /kubedemo/webclient
