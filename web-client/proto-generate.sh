#!/usr/bin/env bash

protoc -I=../api-definition/proto \
    ../api-definition/proto/commons.proto \
    ../api-definition/proto/management_service.proto \
    ../api-definition/proto/band_provider_service.proto \
    --js_out=import_style=commonjs:./src/api/ \
    --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./src/api/
