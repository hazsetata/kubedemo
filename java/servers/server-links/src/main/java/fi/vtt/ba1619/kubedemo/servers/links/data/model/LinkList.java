package fi.vtt.ba1619.kubedemo.servers.links.data.model;

import fi.vtt.ba1619.kubedemo.servers.common.data.model.Link;

import java.util.List;
import java.util.Objects;

public class LinkList {
    private List<Link> links;

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LinkList)) {
            return false;
        }
        LinkList linkList = (LinkList) o;
        return Objects.equals(links, linkList.links);
    }

    @Override
    public int hashCode() {
        return Objects.hash(links);
    }

    @Override
    public String toString() {
        return "LinkList{" +
                "links=" + links +
                '}';
    }
}
