package fi.vtt.ba1619.kubedemo.servers.links.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.vtt.ba1619.kubedemo.servers.common.content.base.AbstractContentManager;
import fi.vtt.ba1619.kubedemo.servers.links.data.model.LinkList;

public class LinkListManager extends AbstractContentManager<String, LinkList> {
    @SuppressWarnings("WeakerAccess")
    public static final String LINK_COLLECTION = "link_collection.json";

    public LinkListManager() {
        this(new ObjectMapper());
    }

    @SuppressWarnings("WeakerAccess")
    public LinkListManager(ObjectMapper objectMapper) {
        super(objectMapper, String.class, LinkList.class, LINK_COLLECTION);
    }
}
