package fi.vtt.ba1619.kubedemo.servers.links.config;

import fi.vtt.ba1619.kubedemo.servers.common.error.ErrorRate;
import fi.vtt.ba1619.kubedemo.servers.links.config.properties.ErrorConfigurationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ErrorConfiguration {
    private static final Logger log = LoggerFactory.getLogger(ErrorConfiguration.class);

    @Bean
    public ErrorRate errorRate(ErrorConfigurationProperties properties) {
        log.debug("Error rate is set to (%): {}", properties.getRate());

        return new ErrorRate(properties.getRate());
    }
}
