package fi.vtt.ba1619.kubedemo.servers.links.grpc;

import fi.vtt.ba1619.kubedemo.api.v1.LinkProviderService;
import fi.vtt.ba1619.kubedemo.api.v1.LinksGrpc;
import fi.vtt.ba1619.kubedemo.servers.common.data.model.Link;
import fi.vtt.ba1619.kubedemo.servers.common.error.ErrorRate;
import fi.vtt.ba1619.kubedemo.servers.links.data.LinkListManager;
import fi.vtt.ba1619.kubedemo.servers.links.data.model.LinkList;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import static fi.vtt.ba1619.kubedemo.servers.common.data.convert.LinkConverter.convertLink;

@GRpcService
public class LinksGrpcService extends LinksGrpc.LinksImplBase {
    private LinkListManager linkListManager;
    private ErrorRate errorRate;

    @Autowired
    public LinksGrpcService(LinkListManager linkListManager, ErrorRate errorRate) {
        this.linkListManager = linkListManager;
        this.errorRate = errorRate;
    }

    @Override
    public void getLinks(LinkProviderService.LinksRequest request, StreamObserver<LinkProviderService.LinksResponse> responseObserver) {
        LinkList links = linkListManager.get(request.getName());

        if ((links != null) && (links.getLinks() != null)){
            if (errorRate.isError()) {
                responseObserver.onError(Status.INTERNAL.withDescription("Demo error").asException());
            }
            else {
                final LinkProviderService.LinksResponse.Builder replyBuilder = LinkProviderService.LinksResponse.newBuilder();

                for (Link link : links.getLinks()) {
                    replyBuilder.addLinks(convertLink(link));
                }

                responseObserver.onNext(replyBuilder.build());
                responseObserver.onCompleted();
            }
        }
        else {
            responseObserver.onError(Status.NOT_FOUND.withDescription("Links for the given name are not available.").asException());
        }
    }
}
