package fi.vtt.ba1619.kubedemo.servers.links.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("kubedemo.errors")
public class ErrorConfigurationProperties {
    /**
     * Error rate (percentage) - to be used demo errors (0 <= x <= 100).
     */
    private int rate = 0;

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
