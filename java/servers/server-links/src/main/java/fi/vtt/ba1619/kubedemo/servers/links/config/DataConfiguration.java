package fi.vtt.ba1619.kubedemo.servers.links.config;

import fi.vtt.ba1619.kubedemo.servers.links.data.LinkListManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataConfiguration {
    private static final Logger log = LoggerFactory.getLogger(DataConfiguration.class);

    @Bean
    public LinkListManager linkListManager() {
        LinkListManager retValue = new LinkListManager();

        log.debug("Loaded link-list with {} entries.", retValue.getEntryCount());

        return retValue;
    }
}
