package fi.vtt.ba1619.kubedemo.servers.art.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.vtt.ba1619.kubedemo.servers.common.content.base.AbstractContentManager;
import fi.vtt.ba1619.kubedemo.servers.common.data.model.Link;

public class ArtManager extends AbstractContentManager<String, Link> {
    @SuppressWarnings("WeakerAccess")
    public static final String ART_COLLECTION = "art_collection.json";

    public ArtManager() {
        this(new ObjectMapper());
    }

    @SuppressWarnings("WeakerAccess")
    public ArtManager(ObjectMapper objectMapper) {
        super(objectMapper, String.class, Link.class, ART_COLLECTION);
    }
}
