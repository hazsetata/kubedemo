package fi.vtt.ba1619.kubedemo.servers.art.grpc;

import fi.vtt.ba1619.kubedemo.api.v1.ArtGrpc;
import fi.vtt.ba1619.kubedemo.api.v1.ArtProviderService;
import fi.vtt.ba1619.kubedemo.servers.art.data.ArtManager;
import fi.vtt.ba1619.kubedemo.servers.common.data.model.Link;
import fi.vtt.ba1619.kubedemo.servers.common.error.ErrorRate;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import static fi.vtt.ba1619.kubedemo.servers.common.data.convert.LinkConverter.convertLink;

@GRpcService
public class ArtGrpcService extends ArtGrpc.ArtImplBase {
    private ArtManager artManager;
    private ErrorRate errorRate;

    @Autowired
    public ArtGrpcService(ArtManager artManager, ErrorRate errorRate) {
        this.artManager = artManager;
        this.errorRate = errorRate;
    }

    @Override
    public void getArt(ArtProviderService.ArtRequest request, StreamObserver<ArtProviderService.ArtResponse> responseObserver) {
        Link artLink = artManager.get(request.getName());

        if (artLink != null) {
            if (errorRate.isError()) {
                responseObserver.onError(Status.INTERNAL.withDescription("Demo error").asException());
            }
            else {
                final ArtProviderService.ArtResponse.Builder replyBuilder = ArtProviderService.ArtResponse.newBuilder();
                replyBuilder.setArtLink(convertLink(artLink));

                responseObserver.onNext(replyBuilder.build());
                responseObserver.onCompleted();
            }
        }
        else {
            responseObserver.onError(Status.NOT_FOUND.withDescription("Art for the given name isn't available.").asException());
        }
    }
}
