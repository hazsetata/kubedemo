package fi.vtt.ba1619.kubedemo.servers.art.config;

import fi.vtt.ba1619.kubedemo.servers.art.data.ArtManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataConfiguration {
    private static final Logger log = LoggerFactory.getLogger(DataConfiguration.class);

    @Bean
    public ArtManager artManager() {
        ArtManager retValue = new ArtManager();

        log.debug("Loaded art-list with {} entries.", retValue.getEntryCount());

        return retValue;
    }
}
