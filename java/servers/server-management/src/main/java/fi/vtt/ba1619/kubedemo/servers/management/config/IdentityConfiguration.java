package fi.vtt.ba1619.kubedemo.servers.management.config;

import fi.vtt.ba1619.kubedemo.servers.management.utils.ServerIdentity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IdentityConfiguration {
    @Bean
    public ServerIdentity serverIdentity() {
        return new ServerIdentity();
    }
}
