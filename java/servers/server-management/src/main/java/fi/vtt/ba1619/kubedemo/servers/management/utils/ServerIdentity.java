package fi.vtt.ba1619.kubedemo.servers.management.utils;

import java.util.UUID;

public class ServerIdentity {
    private final String serverIdentity;

    public ServerIdentity() {
        serverIdentity = UUID.randomUUID().toString();
    }

    public String getValue() {
        return serverIdentity;
    }
}
