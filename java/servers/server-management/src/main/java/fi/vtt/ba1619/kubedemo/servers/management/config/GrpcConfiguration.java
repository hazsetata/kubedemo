package fi.vtt.ba1619.kubedemo.servers.management.config;

import io.grpc.ServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import org.lognet.springboot.grpc.GRpcServerBuilderConfigurer;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class GrpcConfiguration {
    @Component
    public class ManagementServerBuilderConfigurer extends GRpcServerBuilderConfigurer {
        @Override
        public void configure(ServerBuilder<?> serverBuilder){
            serverBuilder.addService(ProtoReflectionService.newInstance());
        }
    }
}
