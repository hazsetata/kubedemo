package fi.vtt.ba1619.kubedemo.servers.management.grpc;

import fi.vtt.ba1619.kubedemo.api.v1.ManagementGrpc;
import fi.vtt.ba1619.kubedemo.api.v1.ManagementService;
import fi.vtt.ba1619.kubedemo.servers.management.utils.ServerIdentity;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;

@GRpcService
public class ManagementGrpcService extends ManagementGrpc.ManagementImplBase {
    private static final String NOT_AVAILABLE = "N/A";

    private BuildProperties buildProperties;
    private GitProperties gitProperties;
    private ServerIdentity serverIdentity;

    @Autowired
    public ManagementGrpcService(BuildProperties buildProperties,
                                 GitProperties gitProperties,
                                 ServerIdentity serverIdentity) {
        this.buildProperties = buildProperties;
        this.gitProperties = gitProperties;
        this.serverIdentity = serverIdentity;
    }

    @Override
    public void getServerInfo(ManagementService.ServerInfoRequest request, StreamObserver<ManagementService.ServerInfoResponse> responseObserver) {
        final ManagementService.ServerInfoResponse.Builder replyBuilder = ManagementService.ServerInfoResponse.newBuilder();
        final ManagementService.ServerInfo.Builder infoBuilder = ManagementService.ServerInfo.newBuilder();

        if (buildProperties != null) {
            infoBuilder.setBuildName(buildProperties.getName());
            infoBuilder.setBuildVersion(buildProperties.getVersion());
        }
        else {
            infoBuilder.setBuildName(NOT_AVAILABLE);
            infoBuilder.setBuildVersion(NOT_AVAILABLE);
        }

        if (gitProperties != null) {
            infoBuilder.setGitBranch(gitProperties.getBranch());
            infoBuilder.setGitCommitId(gitProperties.getCommitId());
        }
        else {
            infoBuilder.setGitBranch(NOT_AVAILABLE);
            infoBuilder.setGitCommitId(NOT_AVAILABLE);
        }

        infoBuilder.setServerInstanceId(serverIdentity.getValue());

        replyBuilder.setServerInfo(infoBuilder.build());
        responseObserver.onNext(replyBuilder.build());
        responseObserver.onCompleted();
    }
}
