package fi.vtt.ba1619.kubedemo.servers.bands.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("kubedemo.demo")
public class DemoProperties {
    /**
     * If true, an alternative set of "bands" will be returned, while if false the original implementation
     * displaying metal bands' info is shown.
     */
    private boolean sensitive = false;

    public boolean isSensitive() {
        return sensitive;
    }

    public void setSensitive(boolean sensitive) {
        this.sensitive = sensitive;
    }
}
