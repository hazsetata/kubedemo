package fi.vtt.ba1619.kubedemo.servers.bands.config.properties;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

public class GrpcClient {
    /**
     * The host to connect to.
     */
    @NotBlank
    private String hostname;

    /**
     * The port to connect to.
     */
    private int port;

    /**
     * If true TLS encryption will be used on the connection.
     */
    private boolean useTls;

    /**
     * If true and useTLs is also true, then validity of the certificates will not be enforced (e.g.: self-signed
     * certificates can be used).
     */
    private boolean insecureTls;

    public GrpcClient() {
    }

    public GrpcClient(@NotBlank String hostname, int port, boolean useTls, boolean insecureTls) {
        this.hostname = hostname;
        this.port = port;
        this.useTls = useTls;
        this.insecureTls = insecureTls;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isUseTls() {
        return useTls;
    }

    public void setUseTls(boolean useTls) {
        this.useTls = useTls;
    }

    public boolean isInsecureTls() {
        return insecureTls;
    }

    public void setInsecureTls(boolean insecureTls) {
        this.insecureTls = insecureTls;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GrpcClient)) {
            return false;
        }
        GrpcClient that = (GrpcClient) o;
        return port == that.port &&
                useTls == that.useTls &&
                insecureTls == that.insecureTls &&
                hostname.equals(that.hostname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hostname, port, useTls, insecureTls);
    }

    @Override
    public String toString() {
        return "GrpcClient{" +
                "hostname='" + hostname + '\'' +
                ", port=" + port +
                ", useTls=" + useTls +
                ", insecureTls=" + insecureTls +
                '}';
    }
}
