package fi.vtt.ba1619.kubedemo.servers.bands.grpc;

import fi.vtt.ba1619.kubedemo.api.v1.BandProviderService;
import fi.vtt.ba1619.kubedemo.api.v1.BandsGrpc;
import fi.vtt.ba1619.kubedemo.api.v1.Commons;
import fi.vtt.ba1619.kubedemo.servers.bands.data.BandsManager;
import fi.vtt.ba1619.kubedemo.servers.bands.data.model.BandInfo;
import fi.vtt.ba1619.kubedemo.tools.grpc.client.ArtProviderGrpcClient;
import fi.vtt.ba1619.kubedemo.tools.grpc.client.ClientException;
import fi.vtt.ba1619.kubedemo.tools.grpc.client.LinkProviderGrpcClient;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

@GRpcService
public class BandsGrpcService extends BandsGrpc.BandsImplBase {
    private static final Logger log = LoggerFactory.getLogger(BandsGrpcService.class);

    private BandsManager bandsManager;
    private ArtProviderGrpcClient artClient;
    private LinkProviderGrpcClient linksClient;

    @Autowired
    public BandsGrpcService(BandsManager bandsManager, ArtProviderGrpcClient artClient, LinkProviderGrpcClient linksClient) {
        this.bandsManager = bandsManager;
        this.artClient = artClient;
        this.linksClient = linksClient;
    }

    @Override
    public void getBands(BandProviderService.BandListRequest request, StreamObserver<BandProviderService.BandListResponse> responseObserver) {
        Set<String> bandNames = bandsManager.getKeys();

        if ((bandNames != null) && (bandNames.size() > 0)) {
            final BandProviderService.BandListResponse.Builder replyBuilder = BandProviderService.BandListResponse.newBuilder();
            for(String bandName: bandNames) {
                BandProviderService.BandInfo bandInfo = convertBand(bandName);

                if (bandInfo != null) {
                    replyBuilder.addBands(bandInfo);
                }
            }

            replyBuilder.setSensitiveDemo(bandsManager.isSensitiveDemo());
            responseObserver.onNext(replyBuilder.build());
            responseObserver.onCompleted();

        }
        else {
            responseObserver.onError(Status.NOT_FOUND.withDescription("Band-list is not available / empty.").asException());
        }
    }

    private BandProviderService.BandInfo convertBand(String bandName) {
        BandInfo band = bandsManager.get(bandName);

        if (band != null) {
            BandProviderService.BandInfo.Builder retValue = BandProviderService.BandInfo.newBuilder();
            retValue.setName(bandName);
            retValue.setDisplayName(band.getDisplayName());
            retValue.setCountryCode(band.getCountryCode());

            boolean hasError = false;

            try {
                Commons.Link artLink = artClient.getArtLinkFor(bandName);
                if (artLink != null) {
                    retValue.setArtLink(artLink);
                }
            }
            catch (ClientException e) {
                // Do nothing more
                log.error("Error retrieving art for: {}", bandName);
                hasError = true;
            }

            try {
                List<Commons.Link> links = linksClient.getLinksFor(bandName);
                if (links != null) {
                    retValue.addAllLinks(links);
                }
            }
            catch (ClientException e) {
                // Do nothing more
                log.error("Error retrieving links for: {}", bandName);
                hasError = true;
            }

            retValue.setUpstreamError(hasError);

            return retValue.build();
        }

        return null;
    }
}
