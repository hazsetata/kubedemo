package fi.vtt.ba1619.kubedemo.servers.bands.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("kubedemo.clients")
public class ClientConfigurationProperties {
    /**
     * gRPC client details to connect to the artwork provider service.
     */
    private GrpcClient art = new GrpcClient("localhost", 6566, false, false);

    /**
     * gRPC client details to connect to the link provider service.
     */
    private GrpcClient links = new GrpcClient("localhost", 6564, false, false);


    public GrpcClient getArt() {
        return art;
    }

    public void setArt(GrpcClient art) {
        this.art = art;
    }

    public GrpcClient getLinks() {
        return links;
    }

    public void setLinks(GrpcClient links) {
        this.links = links;
    }
}
