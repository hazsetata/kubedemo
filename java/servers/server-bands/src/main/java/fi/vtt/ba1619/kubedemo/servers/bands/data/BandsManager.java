package fi.vtt.ba1619.kubedemo.servers.bands.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.vtt.ba1619.kubedemo.servers.bands.data.model.BandInfo;
import fi.vtt.ba1619.kubedemo.servers.common.content.base.AbstractContentManager;

public class BandsManager extends AbstractContentManager<String, BandInfo> {
    @SuppressWarnings("WeakerAccess")
    public static final String BAND_LIST = "band_list.json";
    public static final String SENSITIVE_LIST = "sensitive_list.json";

    private final boolean sensitiveDemo;

    public BandsManager(boolean sensitive) {
        this(sensitive, new ObjectMapper());
    }

    @SuppressWarnings("WeakerAccess")
    public BandsManager(boolean sensitive, ObjectMapper objectMapper) {
        super(objectMapper, String.class, BandInfo.class, sensitive ? SENSITIVE_LIST : BAND_LIST);
        this.sensitiveDemo = sensitive;
    }

    public boolean isSensitiveDemo() {
        return sensitiveDemo;
    }
}
