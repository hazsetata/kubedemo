package fi.vtt.ba1619.kubedemo.servers.bands.config;

import fi.vtt.ba1619.kubedemo.servers.bands.config.properties.ClientConfigurationProperties;
import fi.vtt.ba1619.kubedemo.tools.grpc.client.ArtProviderGrpcClient;
import fi.vtt.ba1619.kubedemo.tools.grpc.client.LinkProviderGrpcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLException;

@Configuration
public class ClientConfiguration {
    private static final Logger log = LoggerFactory.getLogger(ClientConfiguration.class);

    @Bean
    public ArtProviderGrpcClient artProviderGrpcClient(ClientConfigurationProperties properties) throws SSLException {
        log.debug("Art-provider client configuration: {}", properties.getArt());

        return new ArtProviderGrpcClient(
                properties.getArt().getHostname(),
                properties.getArt().getPort(),
                properties.getArt().isUseTls(),
                properties.getArt().isInsecureTls()
        );
    }

    @Bean
    public LinkProviderGrpcClient linkProviderGrpcClient(ClientConfigurationProperties properties) throws SSLException {
        log.debug("Link-provider client configuration: {}", properties.getLinks());

        return new LinkProviderGrpcClient(
                properties.getLinks().getHostname(),
                properties.getLinks().getPort(),
                properties.getLinks().isUseTls(),
                properties.getLinks().isInsecureTls()
        );
    }
}
