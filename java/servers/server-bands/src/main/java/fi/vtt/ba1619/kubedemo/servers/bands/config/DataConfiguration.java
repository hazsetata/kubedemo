package fi.vtt.ba1619.kubedemo.servers.bands.config;

import fi.vtt.ba1619.kubedemo.servers.bands.config.properties.DemoProperties;
import fi.vtt.ba1619.kubedemo.servers.bands.data.BandsManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataConfiguration {
    private static final Logger log = LoggerFactory.getLogger(DataConfiguration.class);

    @Bean
    public BandsManager bandsManager(DemoProperties demoProperties) {
        BandsManager retValue = new BandsManager(demoProperties.isSensitive());

        log.debug("Sensitive demonstration configured: {}", demoProperties.isSensitive());
        log.debug("Loaded bands-list with {} entries.", retValue.getEntryCount());

        return retValue;
    }
}
