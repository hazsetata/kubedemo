package fi.vtt.ba1619.kubedemo.servers.bands.data.model;

import java.util.Objects;

public class BandInfo {
    private String name;
    private String countryCode;
    private String displayName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BandInfo)) {
            return false;
        }
        BandInfo bandInfo = (BandInfo) o;
        return name.equals(bandInfo.name) &&
                countryCode.equals(bandInfo.countryCode) &&
                displayName.equals(bandInfo.displayName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, countryCode, displayName);
    }

    @Override
    public String toString() {
        return "BandInfo{" +
                "name='" + name + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", displayName='" + displayName + '\'' +
                '}';
    }
}
