package fi.vtt.ba1619.kubedemo.servers.common.content.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;
import java.util.Set;

public class ContentHolder<K, V> {
    private Map<K, V> content;

    public Map<K, V> getContent() {
        return content;
    }

    public void setContent(Map<K, V> content) {
        this.content = content;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonIgnore
    public V getContentValue(K key) {
        if (content != null) {
            return content.get(key);
        }

        return null;
    }

    public int getContentEntryCount() {
        if (content != null) {
            return content.size();
        }

        return 0;
    }

    public Set<K> getContentKeys() {
        if (content != null) {
            return content.keySet();
        }

        return null;
    }
}
