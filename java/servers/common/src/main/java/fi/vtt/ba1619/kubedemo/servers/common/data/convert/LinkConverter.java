package fi.vtt.ba1619.kubedemo.servers.common.data.convert;

import fi.vtt.ba1619.kubedemo.api.v1.Commons;
import fi.vtt.ba1619.kubedemo.servers.common.data.model.Link;

public class LinkConverter {
    public static Commons.Link convertLink(Link link) {
        Commons.Link.Builder retValue = Commons.Link.newBuilder();

        retValue.setName(link.getName());
        retValue.setUrl(link.getUrl());

        return retValue.build();
    }
}
