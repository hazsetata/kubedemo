package fi.vtt.ba1619.kubedemo.servers.common.content;

public interface IContentManager<K, V> {
    V get(K key);
}
