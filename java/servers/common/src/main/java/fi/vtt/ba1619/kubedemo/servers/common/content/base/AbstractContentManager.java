package fi.vtt.ba1619.kubedemo.servers.common.content.base;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import fi.vtt.ba1619.kubedemo.servers.common.content.IContentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Set;

public class AbstractContentManager<K, V> implements IContentManager<K, V> {
    private static final Logger log = LoggerFactory.getLogger(AbstractContentManager.class);

    private ContentHolder<K, V> contentHolder;

    public AbstractContentManager(ObjectMapper objectMapper, Class<K> keyClass, Class<V> valueClass, String contentFileName) {
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        JavaType contentHolderType = typeFactory.constructParametricType(
                ContentHolder.class,
                keyClass, valueClass
        );

        InputStream contentInputStream = getOverrideInputStream(contentFileName);
        if (contentInputStream == null) {
            contentInputStream = getInternalInputStream(contentFileName);
        }

        if (contentInputStream == null) {
            log.debug("Internal content file not-found.");
            contentHolder = new ContentHolder<>();
        }
        else {
            try {
                contentHolder = objectMapper.readValue(contentInputStream, contentHolderType);
            }
            catch (IOException e) {
                log.debug("Can't parse content.", e);
                contentHolder = new ContentHolder<>();
            }
            finally {
                try {
                    contentInputStream.close();
                }
                catch (IOException e) {
                    // Can't do anything.
                }
            }
        }
    }

    @SuppressWarnings("WeakerAccess")
    protected InputStream getInternalInputStream(String contentFileName) {
        String resourceName = "/" + contentFileName;
        log.debug("Falling back to internal content file: {}", resourceName);

        return getClass().getResourceAsStream(resourceName);
    }

    @SuppressWarnings("WeakerAccess")
    protected InputStream getOverrideInputStream(String contentFileName) {
        File overrideFile = new File(contentFileName);
        log.debug("Checking content-override file: {}", overrideFile.getAbsolutePath());
        if ((overrideFile.exists()) && (overrideFile.isFile())) {
            try {
                return new FileInputStream(overrideFile);
            }
            catch (FileNotFoundException e) {
                log.debug("Content-override file can't be read.");
            }
        }
        else {
            log.debug("Content-override file doesn't exist.");
        }

        return null;
    }

    @Override
    public V get(K key) {
        return contentHolder.getContentValue(key);
    }

    public int getEntryCount() {
        return contentHolder.getContentEntryCount();
    }

    public Set<K> getKeys() {
        return contentHolder.getContentKeys();
    }
}
