package fi.vtt.ba1619.kubedemo.servers.common.content;

import fi.vtt.ba1619.kubedemo.servers.common.content.impl.TestContent;
import fi.vtt.ba1619.kubedemo.servers.common.content.impl.TestContentManager;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ContentManagerTest {
    @Test
    public void testFirst_works() {
        TestContentManager contentManager = new TestContentManager();
        TestContent first = contentManager.get("first");

        assertThat(first, notNullValue());
        assertThat(first.getDescription(), equalTo("First content"));
        assertThat(first.getValue(), equalTo(1));
    }

    @Test
    public void testSecond_works() {
        TestContentManager contentManager = new TestContentManager();
        TestContent second = contentManager.get("second");

        assertThat(second, notNullValue());
        assertThat(second.getDescription(), equalTo("Second content"));
        assertThat(second.getValue(), equalTo(2));
    }

    @Test
    public void testThird_failes() {
        TestContentManager contentManager = new TestContentManager();
        TestContent third = contentManager.get("third");

        assertThat(third, nullValue());
    }
}