package fi.vtt.ba1619.kubedemo.servers.common.content.impl;

import java.util.Objects;

public class TestContent {
    private String description;
    private int value;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestContent)) {
            return false;
        }
        TestContent that = (TestContent) o;
        return value == that.value &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, value);
    }

    @Override
    public String toString() {
        return "TestContent{" +
                "description='" + description + '\'' +
                ", value=" + value +
                '}';
    }
}
