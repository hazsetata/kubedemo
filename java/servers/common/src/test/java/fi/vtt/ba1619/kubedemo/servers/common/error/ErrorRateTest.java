package fi.vtt.ba1619.kubedemo.servers.common.error;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ErrorRateTest {
    @Test
    public void testZeroWorks() {
        ErrorRate errorRate = new ErrorRate(0);
        assertThat(errorRate.isError(), is(false));
    }

    @Test
    public void testBellowZeroWorks() {
        ErrorRate errorRate = new ErrorRate(-1);
        assertThat(errorRate.isError(), is(false));
    }

    @Test
    public void testHundredWorks() {
        ErrorRate errorRate = new ErrorRate(100);
        assertThat(errorRate.isError(), is(true));
    }

    @Test
    public void testOverHundredWorks() {
        ErrorRate errorRate = new ErrorRate(200);
        assertThat(errorRate.isError(), is(true));
    }

    @Test
    public void testTwentyWorks() {
        ErrorRate errorRate = new ErrorRate(20);
        int errorCount = 0;
        for (int i=0; i < 1000; i++) {
            if (errorRate.isError()) {
                errorCount++;
            }
        }

        assertThat(errorCount, not(equalTo(0)));
        assertThat(errorCount, greaterThanOrEqualTo(100));
        assertThat(errorCount, lessThanOrEqualTo(300));
    }
}