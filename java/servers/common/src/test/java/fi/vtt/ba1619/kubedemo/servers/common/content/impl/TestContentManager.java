package fi.vtt.ba1619.kubedemo.servers.common.content.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.vtt.ba1619.kubedemo.servers.common.content.base.AbstractContentManager;

public class TestContentManager extends AbstractContentManager<String, TestContent> {
    public TestContentManager() {
        super(
                new ObjectMapper(),
                String.class,
                TestContent.class,
                "test-content.json"
        );
    }
}
