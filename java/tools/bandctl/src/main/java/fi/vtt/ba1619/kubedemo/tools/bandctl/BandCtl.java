package fi.vtt.ba1619.kubedemo.tools.bandctl;

import fi.vtt.ba1619.kubedemo.api.v1.BandProviderService;
import fi.vtt.ba1619.kubedemo.api.v1.Commons;
import fi.vtt.ba1619.kubedemo.tools.grpc.client.BandProviderGrpcClient;
import picocli.CommandLine;

import java.util.List;

/**
 * @author Zsolt Homorodi.
 */
@CommandLine.Command(
        name = "bandctl",
        description = "BandControl",
        mixinStandardHelpOptions = true,
        version = "bandctl - v1.0"
)
public class BandCtl implements Runnable{
    private static final String NO_TEXT = "None";

    @CommandLine.Option(names = {"-o", "--host"}, description = "The host to connect to.", required = true)
    private String host;
    @CommandLine.Option(names = {"-p", "--port"}, description = "The port to connect to (default: ${DEFAULT-VALUE}).")
    private int port = 6567;
    @CommandLine.Option(names = {"-t", "--use-tls"}, description = "If TLS should be used for the connection (default: ${DEFAULT-VALUE}).")
    private boolean useTLS = true;
    @CommandLine.Option(names = {"-i", "--insecure-tls"}, description = "If TLS should be insecure (no certificate checking).")
    private boolean insecureTLS = false;

    public static void main(String[] args) {
        BandCtl cli = new BandCtl();
        CommandLine cmd = new CommandLine(cli);
        int exitCode = cmd.execute(args);

        System.exit(exitCode);
    }

    @Override
    public void run() {
        try {
            System.out.println("BandControl");
            System.out.println("[run with -h for usage summary]");
            System.out.println();

            BandProviderGrpcClient client = new BandProviderGrpcClient(host, port, useTLS, insecureTLS);
            List<BandProviderService.BandInfo> bands = client.getBands();
            printBands(bands);
        }
        catch (Exception e) {
            System.err.println("Error starting load-balancer tester.");
            e.printStackTrace();
        }
    }

    private void printBands(List<BandProviderService.BandInfo> bands) {
        if ((bands != null) && (bands.size() > 0)) {
            for (BandProviderService.BandInfo band : bands) {
                System.out.println("====> " + band.getDisplayName() + "<====");
                System.out.println("  Upstream errors: " + (band.getUpstreamError() ? "yes" : "no"));
                System.out.println("  Country: " + band.getCountryCode().toUpperCase());
                System.out.println("  Art    : " + getAsString(band.getArtLink(), false));

                System.out.println("  Links");
                System.out.println("  -----");
                List<Commons.Link> links = band.getLinksList();
                if ((links != null) && (links.size() > 0)) {
                    for (Commons.Link link : links) {
                        System.out.println("    " + getAsString(link, true));
                    }
                }
                else {
                    System.out.println("    No links found.");
                }

                System.out.println();
            }
        }
        else {
            System.out.println("No bands found.");
        }
    }

    private String getAsString(Commons.Link link, boolean printName) {
        if (link != null) {
            StringBuilder retValue = new StringBuilder();
            boolean shouldPrintName = printName && hasText(link.getName());

            if (shouldPrintName) {
                retValue.append(link.getName()).append(" [ ");
            }

            if (hasText(link.getUrl())) {
                retValue.append(link.getUrl());
            }
            else {
                retValue.append(NO_TEXT);
            }

            if (shouldPrintName) {
                retValue.append(" ] ");
            }

            return retValue.toString();
        }
        else {
            return NO_TEXT;
        }
    }

    private boolean hasText(String text) {
        return (text != null) && (!text.trim().equals(""));
    }
}
