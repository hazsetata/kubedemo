package fi.vtt.ba1619.kubedemo.tools.grpc.client.convert;

import fi.vtt.ba1619.kubedemo.api.v1.Commons;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * @author Zsolt Homorodi.
 */
public class TimestampConverter {
    public static Commons.TimeStamp convert(ZonedDateTime dateTime) {
        final Commons.TimeStamp.Builder timeStampBuilder = Commons.TimeStamp.newBuilder();
        timeStampBuilder.setEpochSecond(dateTime.toEpochSecond());
        timeStampBuilder.setNanoOfSecond(dateTime.getNano());

        return timeStampBuilder.build();
    }

    public static ZonedDateTime convert(Commons.TimeStamp timeStamp) {
        return ZonedDateTime.ofInstant(
                Instant.ofEpochSecond(timeStamp.getEpochSecond(), timeStamp.getNanoOfSecond()),
                ZoneOffset.UTC
        );
    }
}
