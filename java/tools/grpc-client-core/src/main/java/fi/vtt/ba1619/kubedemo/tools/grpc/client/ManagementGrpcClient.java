package fi.vtt.ba1619.kubedemo.tools.grpc.client;

import fi.vtt.ba1619.kubedemo.api.v1.ManagementGrpc;
import fi.vtt.ba1619.kubedemo.api.v1.ManagementService;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;

/**
 * Simple client implementation using the gRPC API to interact with the backend management services.
 *
 * @author Zsolt Homorodi.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ManagementGrpcClient extends AbstractGrpcClient{
    private static final Logger log = LoggerFactory.getLogger(ManagementGrpcClient.class);

    private final ManagementGrpc.ManagementBlockingStub grpcStub;

    public ManagementGrpcClient(String host, int port) throws SSLException {
        this(host, port, true, false);
    }

    public ManagementGrpcClient(String host, int port, boolean useTLS, boolean insecureTLS) throws SSLException {
        this(
                getChannelBuilder(host, port, useTLS, insecureTLS)
        );
    }

    public ManagementGrpcClient(ManagedChannelBuilder<?> channelBuilder) {
        super(channelBuilder.build());
        grpcStub = ManagementGrpc.newBlockingStub(channel);
    }

    public ManagementService.ServerInfo getServerInfo() {
        assertChannelOpen();

        log.trace("Reading server-info...");
        ManagementService.ServerInfoRequest request = ManagementService.ServerInfoRequest.newBuilder()
                .build();

        ManagementService.ServerInfoResponse response;
        try {
            response = grpcStub.getServerInfo(request);

            return response.getServerInfo();
        }
        catch (StatusRuntimeException e) {
            throw new ClientException("Error reading server-info.", e);
        }
    }
}
