package fi.vtt.ba1619.kubedemo.tools.grpc.client.convert;

import fi.vtt.ba1619.kubedemo.api.v1.Commons;

/**
 * @author Zsolt Homorodi.
 */
public class DataConverter {
    public static Commons.DoubleValue convert(Double value) {
        Commons.DoubleValue.Builder retValueBuilder = Commons.DoubleValue.newBuilder();
        retValueBuilder.setValue(value);

        return retValueBuilder.build();
    }

    public static Commons.StringValue convert(String value) {
        Commons.StringValue.Builder retValueBuilder = Commons.StringValue.newBuilder();
        retValueBuilder.setValue(value);

        return retValueBuilder.build();
    }
}
