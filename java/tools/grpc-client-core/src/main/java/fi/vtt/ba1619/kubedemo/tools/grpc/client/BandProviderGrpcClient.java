package fi.vtt.ba1619.kubedemo.tools.grpc.client;

import fi.vtt.ba1619.kubedemo.api.v1.BandProviderService;
import fi.vtt.ba1619.kubedemo.api.v1.BandsGrpc;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;
import java.util.List;

public class BandProviderGrpcClient extends AbstractGrpcClient {
    private static final Logger log = LoggerFactory.getLogger(BandProviderGrpcClient.class);

    private final BandsGrpc.BandsBlockingStub grpcStub;

    public BandProviderGrpcClient(String host, int port) throws SSLException {
        this(host, port, true, false);
    }

    public BandProviderGrpcClient(String host, int port, boolean useTLS, boolean insecureTLS) throws SSLException {
        this(
                getChannelBuilder(host, port, useTLS, insecureTLS)
        );
    }

    public BandProviderGrpcClient(ManagedChannelBuilder<?> channelBuilder) {
        super(channelBuilder.build());
        grpcStub = BandsGrpc.newBlockingStub(channel);
    }

    public List<BandProviderService.BandInfo> getBands() {
        assertChannelOpen();

        log.trace("Requesting bands list");
        BandProviderService.BandListRequest request = BandProviderService.BandListRequest.newBuilder()
                .build();

        BandProviderService.BandListResponse response;
        try {
            response = grpcStub.getBands(request);

            return response.getBandsList();
        }
        catch (StatusRuntimeException e) {
            throw new ClientException("Error requesting bands.", e);
        }
    }
}
