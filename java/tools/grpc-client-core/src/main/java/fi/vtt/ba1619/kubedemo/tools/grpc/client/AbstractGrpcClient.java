package fi.vtt.ba1619.kubedemo.tools.grpc.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import io.grpc.netty.shaded.io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;
import java.util.concurrent.TimeUnit;

/**
 * @author Zsolt Homorodi.
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractGrpcClient {
    /**
     * The maximum size of inbound messages (50 MiB)
     */
    public static final int DEFAULT_MAX_INBOUND_MESSAGE_SIZE = 78643200;

    private static final Logger log = LoggerFactory.getLogger(AbstractGrpcClient.class);

    protected final ManagedChannel channel;

    protected AbstractGrpcClient(ManagedChannel channel) {
        this.channel = channel;
    }

    public void shutdown() throws ClientException {
        try {
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            throw new ClientException("Shutdown interrupted.", e);
        }
    }

    protected void assertChannelOpen() {
        if (channel.isShutdown()) {
            throw new IllegalStateException("The client is already shut down.");
        }
    }

    protected static ManagedChannelBuilder<?> getChannelBuilder(String host, int port, boolean useTLS, boolean insecureTLS) throws SSLException {
        ManagedChannelBuilder<?> retValue = NettyChannelBuilder
                .forAddress(host, port)
                .maxInboundMessageSize(DEFAULT_MAX_INBOUND_MESSAGE_SIZE);

        if (!useTLS) {
            retValue = retValue.usePlaintext();
        }
        else if (insecureTLS) {
            retValue = ((NettyChannelBuilder) retValue).sslContext(
                    GrpcSslContexts.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build()
            );
        }

        return retValue;
    }
}
