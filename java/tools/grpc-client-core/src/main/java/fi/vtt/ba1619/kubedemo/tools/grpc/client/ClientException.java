package fi.vtt.ba1619.kubedemo.tools.grpc.client;

/**
 * The exception is thrown, when some problem occurs in the client methods (e.g.: gRPC error, thread interruption,
 * etc.).
 */
public class ClientException extends RuntimeException {
    public ClientException(String message) {
        super(message);
    }

    public ClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
