package fi.vtt.ba1619.kubedemo.tools.grpc.client;

import fi.vtt.ba1619.kubedemo.api.v1.ArtGrpc;
import fi.vtt.ba1619.kubedemo.api.v1.ArtProviderService;
import fi.vtt.ba1619.kubedemo.api.v1.Commons;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;

public class ArtProviderGrpcClient extends AbstractGrpcClient {
    private static final Logger log = LoggerFactory.getLogger(ArtProviderGrpcClient.class);

    private final ArtGrpc.ArtBlockingStub grpcStub;

    public ArtProviderGrpcClient(String host, int port) throws SSLException {
        this(host, port, true, false);
    }

    public ArtProviderGrpcClient(String host, int port, boolean useTLS, boolean insecureTLS) throws SSLException {
        this(
                getChannelBuilder(host, port, useTLS, insecureTLS)
        );
    }

    public ArtProviderGrpcClient(ManagedChannelBuilder<?> channelBuilder) {
        super(channelBuilder.build());
        grpcStub = ArtGrpc.newBlockingStub(channel);
    }

    public Commons.Link getArtLinkFor(String name) {
        assertChannelOpen();

        log.trace("Requesting art for: {}", name);
        ArtProviderService.ArtRequest request = ArtProviderService.ArtRequest.newBuilder()
                .setName(name)
                .build();

        ArtProviderService.ArtResponse response;
        try {
            response = grpcStub.getArt(request);

            return response.getArtLink();
        }
        catch (StatusRuntimeException e) {
            throw new ClientException("Error requesting art.", e);
        }
    }

    public String getArtUrlFor(String name) {
        Commons.Link artLink = getArtLinkFor(name);

        return artLink.getUrl();
    }
}
