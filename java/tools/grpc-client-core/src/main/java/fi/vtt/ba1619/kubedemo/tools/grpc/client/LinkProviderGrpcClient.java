package fi.vtt.ba1619.kubedemo.tools.grpc.client;

import fi.vtt.ba1619.kubedemo.api.v1.Commons;
import fi.vtt.ba1619.kubedemo.api.v1.LinkProviderService;
import fi.vtt.ba1619.kubedemo.api.v1.LinksGrpc;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;
import java.util.List;

public class LinkProviderGrpcClient extends AbstractGrpcClient {
    private static final Logger log = LoggerFactory.getLogger(LinkProviderGrpcClient.class);

    private final LinksGrpc.LinksBlockingStub grpcStub;

    public LinkProviderGrpcClient(String host, int port) throws SSLException {
        this(host, port, true, false);
    }

    public LinkProviderGrpcClient(String host, int port, boolean useTLS, boolean insecureTLS) throws SSLException {
        this(
                getChannelBuilder(host, port, useTLS, insecureTLS)
        );
    }

    public LinkProviderGrpcClient(ManagedChannelBuilder<?> channelBuilder) {
        super(channelBuilder.build());
        grpcStub = LinksGrpc.newBlockingStub(channel);
    }

    public List<Commons.Link> getLinksFor(String name) {
        assertChannelOpen();

        log.trace("Requesting links for: {}", name);
        LinkProviderService.LinksRequest request = LinkProviderService.LinksRequest.newBuilder()
                .setName(name)
                .build();

        LinkProviderService.LinksResponse response;
        try {
            response = grpcStub.getLinks(request);

            return response.getLinksList();
        }
        catch (StatusRuntimeException e) {
            throw new ClientException("Error requesting links.", e);
        }
    }
}
