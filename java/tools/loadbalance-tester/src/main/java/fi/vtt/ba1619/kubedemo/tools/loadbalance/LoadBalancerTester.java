package fi.vtt.ba1619.kubedemo.tools.loadbalance;

import fi.vtt.ba1619.kubedemo.api.v1.ManagementService;
import fi.vtt.ba1619.kubedemo.tools.grpc.client.ManagementGrpcClient;
import picocli.CommandLine;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Zsolt Homorodi.
 */
@CommandLine.Command(
        name = "loadbalancer-tester",
        description = "Load-balancer tester",
        mixinStandardHelpOptions = true,
        version = "loadbalancer-tester - v0.1"
)
public class LoadBalancerTester implements Runnable{
    @CommandLine.Option(names = {"-o", "--host"}, description = "The host to connect to.", required = true)
    private String host;
    @CommandLine.Option(names = {"-p", "--port"}, description = "The port to connect to (default: ${DEFAULT-VALUE}).")
    private int port = 6565;
    @CommandLine.Option(names = {"-t", "--use-tls"}, description = "If TLS should be used for the connection (default: ${DEFAULT-VALUE}).")
    private boolean useTLS = true;
    @CommandLine.Option(names = {"-i", "--insecure-tls"}, description = "If TLS should be insecure (no certificate checking).")
    private boolean insecureTLS = false;

    @CommandLine.Option(names = {"-r", "--repeat-count"}, description = "The number of times a chunk-size request-burst will be repeated (default: ${DEFAULT-VALUE}).")
    private int repeatCount = 10;
    @CommandLine.Option(names = {"-c", "--chunk-size"}, description = "The number of consecutive requests in a chunk (default: ${DEFAULT-VALUE}).")
    private int chunkSize = 100;
    @CommandLine.Option(names = {"-d", "--delay"}, description = "The delay (seconds) between the chunks (default: ${DEFAULT-VALUE}).")
    private int delay = 1;

    private Map<String, Integer> serverHitMap = new HashMap<>();

    public static void main(String[] args) {
        LoadBalancerTester cli = new LoadBalancerTester();
        CommandLine cmd = new CommandLine(cli);
        int exitCode = cmd.execute(args);

        System.exit(exitCode);
    }

    @Override
    public void run() {
        try {
            System.out.println("Load-balancer tester");
            System.out.println("[run with -h for usage summary]");
            System.out.println();

            long startTime = System.currentTimeMillis();

            ManagementGrpcClient client = new ManagementGrpcClient(host, port, useTLS, insecureTLS);

            for (int repeat = 0; repeat < repeatCount; repeat++) {
                if (repeat != 0) {
                    if (delay > 0) {
                        System.out.println("Waiting between repeats...");
                        Thread.sleep(delay * 1000);
                    }
                }

                System.out.println("Running request chunk " + (repeat + 1) + "/" + repeatCount);

                for (int chunk = 0; chunk < chunkSize; chunk++) {
                    ManagementService.ServerInfo info = client.getServerInfo();
                    serverHitMap.merge(info.getServerInstanceId(), 1, (a, b) -> a + b);
                }
            }

            long endTime = System.currentTimeMillis();

            printHitMap(endTime - startTime);
        }
        catch (Exception e) {
            System.err.println("Error starting load-balancer tester.");
            e.printStackTrace();
        }
    }

    private void printHitMap(long executionTime) {
        System.out.println();
        System.out.println("Server hit-map");
        System.out.println("==============");
        int totalRequests = repeatCount * chunkSize;
        for(Map.Entry<String, Integer> entry: serverHitMap.entrySet()) {
            System.out.println("  ID: " + entry.getKey() + ", hits: " + entry.getValue() + " (" + (entry.getValue() * 100 / totalRequests) + "%)");
        }
        System.out.println();
        System.out.println("Finished in " + executionTime + "ms");
    }
}
