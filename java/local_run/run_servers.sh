#!/usr/bin/env bash

# Triggered when the user interrupts the script to stop it.
trap quitjobs INT
quitjobs() {
    echo ""
    pkill -P $$
    echo "Killed all running jobs".
    scriptCancelled="true"
    trap - INT
    exit
}

# Wait for user input so the jobs can be quit afterwards.
scriptCancelled="false"
waitforcancel() {
    while :
    do
        if [ "$scriptCancelled" == "true" ]; then
            return
        fi
        sleep 1
    done
}

java -jar ../servers/server-art/build/libs/art-0.4.0.DEV-5e6920b.jar & \
java -jar ../servers/server-links/build/libs/links-0.4.0.DEV-5e6920b.jar & \
java -jar ../servers/server-bands/build/libs/bands-0.4.0.DEV-5e6920b.jar & \
java -jar ../servers/server-management/build/libs/management-0.4.0.DEV-5e6920b.jar

# Trap the input and wait for the script to be cancelled.
waitforcancel
return 0
