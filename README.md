# Welcome to VTT BA1619's Kubernetes Demonstration

This demonstration was developed originally for my KubeCon EU 2019 talk,
and later modified to be used for internal training and demonstrations. 

For detailed documentation visit: [https://hazsetata.gitlab.io/kubedemo](https://hazsetata.gitlab.io/kubedemo).
