# Automated provisioning

This project sets up the pipeline components on a Kubernetes cluster. To be able to use it, 
please follow the steps bellow.

## Prerequisites

This provisioning project uses [Pulumi](https://pulumi.io/) and [Helm](https://helm.sh/) 
to create and configure a new Azure AKS cluster with all the necessary components. The
next paragraphs will describe what tools / tool configurations must be done for this 
project to function correctly.

### Install the Azure CLI 2.0

The installation instructions can be found on 
[this page](https://docs.microsoft.com/en-us/cli/azure/interactive-azure-cli?view=azure-cli-latest). 
Once the client is installed, we need to log-in to our Azure account:

```console
az login
```

### Install and configure Pulumi

Please follow the install instructions on the [Pulumi installation](https://pulumi.io/install/)
page. Once installed, Pulumi needs to be configured for the Azure provider, this process
is described on [this page](https://pulumi.io/install/azure.html). I only tried the
*CLI Authentication* mode (this uses the CLI we installed in the previous step).

### Install and configure Helm

Several components of this installation uses official Helm charts, and this functionality
requires the Helm client to be installed. Helm binaries can be found on the project's
[GitHub page](https://github.com/helm/helm). If you are not using a platform-installer
package, please place the binary in a directory on your PATH.

```console
helm version
```

We will use Helm in a client-only mode (the server-side Tiller component won't be used), so
we have to configure it accordingly:

```console
helm init --client-only
```

The installation scripts use some Helm charts that are not in the default Helm chart 
repository, these repositories have to be added:

Jetstack's [Cert-Manager](https://docs.cert-manager.io/en/release-0.7/index.html): 

```console
helm repo add jetstack https://charts.jetstack.io
```

Once all the repositories are configured, we should update the repository information:

```console
helm repo update
```

### Configure project values

There are some configuration options defined for this project (all collected in `config.ts` 
file). Most of them has sensible defaults, but the security related options should be set 
by you.

#### Standard configuration

Option name | Default value | Secret | Description
------------|---------------|--------|------------
azure:environment|**None**|no|The kind of Azure we will be using. For most users this should be `public`.
location|North Europe|no|The Azure location where the components will be created.
acmeEmail|**None**|no|The email to be used for LetsEncrypt registration.

#### Azure AKS configuration

Option name | Default value | Secret | Description
------------|---------------|--------|------------
vttazureaks:servicePrincipalPassword|**None**|yes|The password the service principal (created by this script) will have.

Secret values (e.g.: servicePrincipalPassword) should be set like this:

```
pulumi config set servicePrincipalPassword --secret <YOUR_PASSWORD_HERE>
``` 

Standard values can be set like this:

```
pulumi config set azure:environment public
``` 

File values can be set like this:

```
pulumi config set sshPublicKey < RSA_Key.pub
``` 

## Reproducible provision runs
### Sealed Secrets
By default Sealed Secrets generates a new private-public key pair, if - when the controller
starts - the key doesn't exist in the cluster. To make the provision reproducible this key 
should be saved after the first run, and re-used in any subsequent runs:

```
kubectl get secret -n kube-system sealed-secrets-key -o yaml > master.key
```  

Once saved clean-up the file (unnecessary metadata) and keep it in a very safe place! To 
set the script up to use this file for future runs, this configuration setting needs to be set:

Option name | Default value | Secret | Description
------------|---------------|--------|------------
vttsealedsecrets:masterKeyPath|**None**|?|The path to the master-key file.

### Weaveworks Flux
By default Weaveworks Flux generates a new private-public key pair, if - when the controller
starts - the key doesn't exist in the cluster. To make the provision reproducible this key 
should be saved after the first run, and re-used in any subsequent runs:

```
kubectl get secret flux-git-deploy -o yaml > flux-git.key
```  

Once saved clean-up the file (unnecessary metadata) and keep it in a very safe place! To 
set the script up to use this file for future runs, this configuration setting needs to be set:

Option name | Default value | Secret | Description
------------|---------------|--------|------------
vttweaveflux:gitIdentityKeyPath|**None**|?|The path to the Git identity file.

## Troubleshooting

### Azure

#### Permission error
If you see an error message similar to:

```
Message="The client '<YOUR_EMAIL>' with object id '...' does not have authorization to 
perform action 'Microsoft.ContainerService/managedClusters/delete' over scope 
'/subscriptions/.../resourceGroups/.../providers/Microsoft.ContainerService/managedClusters/...'."
```

One possible reason is that you have multiple subscriptions, and the wrong one is the default
(Pulumi always uses the default one). You can list your subscriptions like this:

```
az account list --output table
```

and change it like this:

```
az account set --subscription <A_VALUE_FROM_THE_NAME_COLUMN_ABOVE>
```

#### ServicePrincipal not found
Occasionally there will be an error, where the message contains the text (around the middle of the error message):

```
Failure sending request: StatusCode=400 -- Original Error: Code="ServicePrincipalNotFound"
``` 

This happens because the API call that creates the ServicePrincipal returns before it is ready to be used.
It is not a real error, simply run `pulumi up` again and the provisioning will continue.

### Pulumi stuck in an error state

In case the stack gets into a stuck state, where even `pulumi destroy` can't handle it,
it is possible to edit the state:

```
pulumi stack export > state.json
```

then edit this file (e.g.: manually remove the resource that Pulumi got stuck on), and then
re-import the state:

```
pulumi stack import < state.json
```
