import * as pulumi from "@pulumi/pulumi";

export abstract class AcmeProviderResource extends pulumi.ComponentResource {
    public stagingIssuerName: pulumi.Output<string>;
    public stagingProviderName: pulumi.Output<string>;
    public productionIssuerName: pulumi.Output<string>;
    public productionProviderName: pulumi.Output<string>;
}