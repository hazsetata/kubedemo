import * as pulumi from "@pulumi/pulumi";

const config = new pulumi.Config();

export const applicationName = config.get("name") || "VTTResearch";
export const kubernetesType = config.get("kubernetesType") || "digitalocean";
export const installIngress = config.getBoolean("installIngress") || false;
export const ingressType = config.get("ingressType") || "ambassador";
export const dnsType = config.get("dnsType") || "cloudflare";
export const acmeEmail = config.require("acmeEmail");
export const acmeProduction = config.getBoolean("acmeProduction") || false;
export const installArgo = config.getBoolean("installArgo") || false;
export const installGitOpsTools = config.getBoolean("installGitOpsTools") || false;
