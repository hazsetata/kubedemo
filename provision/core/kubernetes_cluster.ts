import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface KubernetesCluster {
    readonly kubernetesProvider: k8s.Provider;
    readonly clusterName: pulumi.Output<string>;
    readonly clusterFQDN: pulumi.Output<string>;
    readonly clusterCredentialsCommand: pulumi.Output<string>;
}
