import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import {ComponentResource} from "@pulumi/pulumi";

export abstract class IngressControllerResource extends ComponentResource {
    public ingressControllerService: pulumi.Output<k8s.core.v1.Service>;
    public ingressControllerNamespace: pulumi.Output<string>;
    public ingressIps: pulumi.Output<string[]>;
}