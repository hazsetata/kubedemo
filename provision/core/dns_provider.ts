import * as pulumi from "@pulumi/pulumi";

export abstract class DnsProviderResource extends pulumi.ComponentResource {
    public fullDomainName: pulumi.Output<string>;
}