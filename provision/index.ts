import * as pulumi from "@pulumi/pulumi";
import * as config from "./core/config"
import {KubernetesCluster} from "./core/kubernetes_cluster";
import {IngressControllerResource} from "./core/ingress_controller";
import {DnsProviderResource} from "./core/dns_provider";

import {CertManagerDeployment} from "./kubernetes/cert-manager/cert-manager"

import {CloudflareDnsProvider} from "./cloudflare/cloudflare_dns";
import {ArgoDeployment} from "./kubernetes/argo/argo";
import {Linkerd2Deployment} from "./kubernetes/linkerd2/linkerd2";
import {AmbassadorIngressDeployment} from "./kubernetes/ingress-ambassador/ingress-ambassador";
import {AcmeProviderResource} from "./core/acme_provider";
import {CloudflareAcmeDnsProvider} from "./cloudflare/cloudflare_acme_dns";
import {WeaveFluxDeployment} from "./kubernetes/weave_flux/weave_flux";
import {SealedSecretsDeployment} from "./kubernetes/sealed_secrets/sealed-secrets";
import {DigitalOceanKubernetesCluster} from "./digitalocean/do_kubernetes_cluster";
import {CustomKubernetesCluster} from "./custom/custom_kubernetes_cluster";
import {WeaveFlaggerDeployment} from "./kubernetes/weave_flagger/weave_flagger";
import {Resource} from "@pulumi/pulumi";
import {FluxProxyDeployment} from "./kubernetes/flux_proxy/fluxproxy";

let kubernetesCluster : KubernetesCluster;

switch (config.kubernetesType) {
    case "digitalocean" : {
        kubernetesCluster = new DigitalOceanKubernetesCluster(
            "k8sCluster",
            {},
            {}
        );
        break;
    }
    case "custom" : {
        kubernetesCluster = new CustomKubernetesCluster(
            "k8sCluster",
            {},
            {}
        );
        break;
    }
    default : {
        throw new Error('Unsupported cloud-provider was selected. Valid values are: [digitalocean, custom]');
    }
}

export let clusterName = kubernetesCluster.clusterName;
export let clusterFQDN = kubernetesCluster.clusterFQDN;
export let clusterCredentialsCommand = kubernetesCluster.clusterCredentialsCommand;

let linkerdDeployment = new Linkerd2Deployment("linkerd",
    {},
    {
        providers: {
            kubernetes: kubernetesCluster.kubernetesProvider
        },
        dependsOn: [ kubernetesCluster.kubernetesProvider ]
    }
);

export let ingressIps: pulumi.Output<string[]>;
export let publicDnsName: pulumi.Output<string>;

let gitOpsDependencies: Resource[] = [];
gitOpsDependencies.push(kubernetesCluster.kubernetesProvider);

if (config.installIngress) {
    let certManagerDeployment = new CertManagerDeployment("cert-manager",
        {},
        {
            providers: {
                kubernetes: kubernetesCluster.kubernetesProvider
            },
            dependsOn: [ kubernetesCluster.kubernetesProvider ]
        }
    );

    let ingressController : IngressControllerResource;
    switch (config.ingressType) {
        case "ambassador" : {
            ingressController = new AmbassadorIngressDeployment("ingress",
                {},
                {
                    providers: {
                        kubernetes: kubernetesCluster.kubernetesProvider
                    },
                    dependsOn: [ certManagerDeployment, linkerdDeployment ]
                }
            );
            break;
        }
        default : {
            throw new Error('Unsupported ingress-provider was selected. Valid values are: [ambassador]');
        }
    }

    ingressIps = ingressController.ingressIps;
    gitOpsDependencies.push(ingressController);

    let dnsProvider : DnsProviderResource;
    switch (config.dnsType) {
        case "cloudflare" : {
            dnsProvider = new CloudflareDnsProvider("dns",
                {
                    ips: ingressIps
                },
                {
                    providers: {
                        kubernetes: kubernetesCluster.kubernetesProvider
                    },
                    dependsOn: [ ingressController ]
                }
            );
            break;
        }
        default : {
            throw new Error('Unsupported dns-provider was selected. Valid values are: [cloudflare]');
        }
    }

    publicDnsName = dnsProvider.fullDomainName;

    let acmeProvider: AcmeProviderResource;
    switch (config.dnsType) {
        case "cloudflare": {
            acmeProvider = new CloudflareAcmeDnsProvider("acme",
                {
                    domainName: publicDnsName,
                    acmeEmail: config.acmeEmail,
                    acmeStaging: !config.acmeProduction,
                    certificateNamespace: ingressController.ingressControllerNamespace
                },
                {
                    providers: {
                        kubernetes: kubernetesCluster.kubernetesProvider
                    },
                    dependsOn: [ certManagerDeployment, ingressController, dnsProvider ]
                }
            );
            break;
        }
    }
}

if (config.installGitOpsTools) {
    let sealedSecretsDeployment = new SealedSecretsDeployment("sealed-secrets",
        {},
        {
            providers: {
                kubernetes: kubernetesCluster.kubernetesProvider
            },
            dependsOn: [ ...gitOpsDependencies ]
        }
    );

    let flaggerDeployment = new WeaveFlaggerDeployment("flagger",
        {},
        {
            providers: {
                kubernetes: kubernetesCluster.kubernetesProvider
            },
            dependsOn: [ ...gitOpsDependencies, sealedSecretsDeployment ]
        }
    );

    let fluxProxyDeployment = new FluxProxyDeployment("fluxproxy",
        {},
        {
            providers: {
                kubernetes: kubernetesCluster.kubernetesProvider
            },
            dependsOn: [ ...gitOpsDependencies, sealedSecretsDeployment ]
        }
    );

    let fluxDeployment = new WeaveFluxDeployment("flux",
        {},
        {
            providers: {
                kubernetes: kubernetesCluster.kubernetesProvider
            },
            dependsOn: [ ...gitOpsDependencies, flaggerDeployment, fluxProxyDeployment ]
        }
    );
}

if (config.installArgo) {
    let argoDeployment = new ArgoDeployment("argo",
        {
            kubernetesMasterServerFqdn: kubernetesCluster.clusterFQDN
        },
        {
            providers: {
                kubernetes: kubernetesCluster.kubernetesProvider
            },
            dependsOn: [ kubernetesCluster.kubernetesProvider ]
        }
    );
}
