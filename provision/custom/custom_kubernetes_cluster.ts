import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as coreConfig from "../core/config"
import {KubernetesCluster} from "../core/kubernetes_cluster";

export interface CustomKubernetesClusterDeploymentArgs {
}

export class CustomKubernetesCluster extends pulumi.ComponentResource implements KubernetesCluster {
    readonly kubernetesProvider: k8s.Provider;
    readonly clusterName: pulumi.Output<string>;
    readonly clusterFQDN: pulumi.Output<string>;
    readonly clusterCredentialsCommand: pulumi.Output<string>;

    constructor(name: string, args: CustomKubernetesClusterDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:core:CustomKubernetesCluster", name, {}, opts);

        this.kubernetesProvider = new k8s.Provider(`${coreConfig.applicationName}-K8S-Provider`,
            {},
            {
                parent: this
            }
        );

        this.clusterName = pulumi.output("custom");
        this.clusterFQDN = pulumi.output("");
        this.clusterCredentialsCommand = pulumi.output("");

        this.registerOutputs({
            kubernetesProvider: this.kubernetesProvider,
            clusterName: this.clusterName,
            clusterFQDN: this.clusterFQDN,
            clusterCredentialsCommand: this.clusterCredentialsCommand
        });
    }
}