import {Base64} from "js-base64";

export function hasText(text: string | undefined): boolean {
    return !((text === undefined) || (text === null) || (text === ""));
}

export function createDockerConfigJsonValue(registryName: string, userName: string, password: string, email: string = ''): string {
    let authString = userName + ":" + password;
    let authEncoded = Base64.encode(authString);

    let dockerObject = {
        "auths": {
            [registryName]: {
                "auth": authEncoded,
                "email": email
            }
        }
    };

    let dockerString = JSON.stringify(dockerObject);

    return Base64.encode(dockerString);
}