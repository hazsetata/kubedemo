import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface WeaveFlaggerDeploymentArgs {
}

export class WeaveFlaggerDeployment extends pulumi.ComponentResource {
    constructor(name: string, args: WeaveFlaggerDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:WeaveFlaggerDeployment", name, {}, opts);

        const weaveFlaggerCRDsCG = new k8s.yaml.ConfigGroup("flagger-crd",
            {
                files: "kubernetes/weave_flagger/yaml/crd.yaml"
            },
            {
                parent: this
            }
        );

        const weaveFlaggerCG = new k8s.yaml.ConfigGroup(`${name}-manifests`,
            {
                files: [
                    "kubernetes/weave_flagger/yaml/flagger_install.yaml"
                ]
            },
            {
                parent: this,
                dependsOn: [ weaveFlaggerCRDsCG ]
            }
        );
    }
}
