import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import {hasText, createDockerConfigJsonValue} from "../../utils/textutils";

export interface ImagePullSecretArgs {
    dockerConfigJsonValue?: string,
    registryName?: string,
    userName?: string,
    password?: string,
    email?: string,
    namespace?: string
}

export class ImagePullSecret extends pulumi.ComponentResource {
    constructor(name: string, args: ImagePullSecretArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:ImagePullSecret", name, {}, opts);

        let secretContent: string;

        if (hasText(args.dockerConfigJsonValue)) {
            secretContent = `${args.dockerConfigJsonValue}`;
        }
        else if ((args.registryName != undefined) && (args.userName != undefined) && (args.password != undefined)){
            secretContent = createDockerConfigJsonValue(
                args.registryName,
                args.userName,
                args.password,
                args.email
            )
        }
        else {
            throw new Error("When creating ImagePullSecret either dockerConfigJsonValue must be specified or all of: registryName, userName and password.");
        }

        let pullSecret = new k8s.core.v1.Secret(`${name}-ips`,
            {
                metadata: {
                    name: `${name}`,
                    namespace: hasText(args.namespace) ? args.namespace : "default"
                },
                type: "kubernetes.io/dockerconfigjson",
                data: {
                    ".dockerconfigjson": secretContent
                }
            },
            {
                parent: this
            }
        );
    }
}