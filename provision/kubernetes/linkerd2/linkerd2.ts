import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface Linkerd2DeploymentArgs {
}

export class Linkerd2Deployment extends pulumi.ComponentResource {
    constructor(name: string, args: Linkerd2DeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:Linkerd2Deployment", name, {}, opts);

        let linkerdCG: k8s.yaml.ConfigGroup;

        linkerdCG = new k8s.yaml.ConfigGroup("linkerd-cg",
            {
                files: "kubernetes/linkerd2/yaml/*.yaml"
            },
            {
                parent: this
            }
        );
    }
}
