# Instructions

The yaml file was created from the official [RBAC enabled yaml](https://getambassador.io/yaml/ambassador/ambassador-rbac.yaml).

Once downloaded we have to modify two parts. We need to add the `namespace: ambassador` field
wherever necessary, and we need to modify the default configuration: enable TLS and 
grpc-web support. For this we need to modify the annotation on the Service named 
**ambassador** (there is another Service defined named **ambassador-admin**
, don't edit that one). The annotation should look like this:
 
```yaml
  annotations:
    getambassador.io/config: |
      ---
      apiVersion: ambassador/v1
      kind: Module
      name: ambassador
      config:
        enable_grpc_web: true
        add_linkerd_headers: true
        cors:
          origins: "*"
          methods: GET,PUT,DELETE,POST,OPTIONS
          headers: keep-alive,user-agent,cache-control,content-type,content-transfer-encoding,x-auth-token,x-accept-content-transfer-encoding,x-accept-response-streaming,x-user-agent,x-grpc-web,grpc-timeout
          exposed_headers: x-auth-token,grpc-status,grpc-message
      ---
      apiVersion: ambassador/v1
      kind: Module
      name: tls
      config:
        server:
          enabled: True
          secret: ingress-secret
          alpn_protocols: h2,http/1.1
``` 
