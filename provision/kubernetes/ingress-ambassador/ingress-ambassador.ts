import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import {IngressControllerResource} from "../../core/ingress_controller";

export interface AmbassadorIngressDeploymentArgs {
}

export class AmbassadorIngressDeployment extends IngressControllerResource {
    constructor(name: string, args: AmbassadorIngressDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:AmbassadorIngressDeployment", name, {}, opts);

        const ambassadorCrdCG = new k8s.yaml.ConfigGroup(`${name}-crd-manifests`,
            {
                files: "kubernetes/ingress-ambassador/yaml/crd.yaml"
            },
            {
                parent: this
            }
        );

        const ambassadorNamespace = new k8s.core.v1.Namespace(`${name}-namespace`,
            {
                metadata: {
                    name: "ambassador",
                    annotations: {
                        "linkerd.io/inject": "enabled"
                    }
                }
            },
            {
                parent: this,
                dependsOn: [ ambassadorCrdCG ]
            }
        );

        const ambassadorCG = new k8s.yaml.ConfigGroup(`${name}-manifests`,
            {
                files: [
                    "kubernetes/ingress-ambassador/yaml/ambassador-rbac.yaml",
                    "kubernetes/ingress-ambassador/yaml/ambassador-service.yaml",
                    "kubernetes/ingress-ambassador/yaml/ambassador-config.yaml"
                ]
            },
            {
                parent: this,
                dependsOn: [ ambassadorCrdCG, ambassadorNamespace ]
            }
        );

        this.ingressControllerService = ambassadorCG.getResource("v1/Service", "ambassador/ambassador");
        this.ingressControllerNamespace = pulumi.output("ambassador");
        this.ingressIps = this.ingressControllerService.apply(
            service => service.status.apply(
                status => status.loadBalancer.ingress.map(function (ingress) {
                    return ingress.ip;
                })
            )
        );

        super.registerOutputs({
            ingressControllerService: this.ingressControllerService,
            ingressControllerNamespace: this.ingressControllerNamespace,
            ingressIps: this.ingressIps
        });
    }
}