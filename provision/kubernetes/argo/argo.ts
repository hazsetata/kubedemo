import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface ArgoDeploymentArgs {
}

export class ArgoDeployment extends pulumi.ComponentResource {
    constructor(name: string, args: ArgoDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:ArgoDeployment", name, {}, opts);

        const argoNamespace = new k8s.core.v1.Namespace("argo-ns",
            {
                metadata: {
                    name: "argo"
                }
            },
            {
                parent: this
            }
        );

        const argoCG = new k8s.yaml.ConfigGroup("argo-cg",
            {
                files: "kubernetes/argo/yaml/*.yaml",
            },
            {
                parent: this,
                dependsOn: [argoNamespace]
            }
        );
    }
}
