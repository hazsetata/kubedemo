import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface CertManagerDeploymentArgs {
}

export class CertManagerDeployment extends pulumi.ComponentResource {
    constructor(name: string, args: CertManagerDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:CertManagerDeployment", name, {}, opts);

        const certManagerNamespace = new k8s.core.v1.Namespace("cert-manager-ns",
            {
                metadata: {
                    name: "cert-manager",
                    labels: {
                        "certmanager.k8s.io/disable-validation": "true"
                    }
                }
            },
            {
                parent: this
            }
        );

        const certManagerCRDsCG = new k8s.yaml.ConfigGroup("cert-manager-crd",
            {
                files: "kubernetes/cert-manager/yaml/crd.yaml"
            },
            {
                parent: this,
                dependsOn: [certManagerNamespace]
            }
        );

        const certManagerChart = new k8s.helm.v2.Chart("cert-manager",
            {
                repo: "jetstack",
                chart: "cert-manager",
                version: "v0.11.0",
                namespace: "cert-manager",
                values: {
                    webhook: {
                        enabled: false
                    }
                }
            },
            {
                parent: this,
                dependsOn: [ certManagerNamespace, certManagerCRDsCG ]
            }
        );
    }
}
