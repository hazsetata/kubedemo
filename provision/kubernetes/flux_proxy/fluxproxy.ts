import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface FluxProxyDeploymentArgs {
}

export class FluxProxyDeployment extends pulumi.ComponentResource {
    constructor(name: string, args: FluxProxyDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:FluxProxyDeployment", name, {}, opts);

        let fluxProxyCG: k8s.yaml.ConfigGroup;

        fluxProxyCG = new k8s.yaml.ConfigGroup("fluxproxy-cg",
            {
                files: "kubernetes/flux_proxy/yaml/*.yaml"
            },
            {
                parent: this
            }
        );
    }
}
