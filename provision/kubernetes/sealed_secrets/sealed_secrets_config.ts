import * as pulumi from "@pulumi/pulumi";

export class SealedSecretsConfig {
    readonly masterKeyPath?: string;

    constructor() {
        const theConfig = new pulumi.Config("vttsealedsecrets");

        this.masterKeyPath = theConfig.get("masterKeyPath");
    }
}

export function getSealedSecretsConfig(): SealedSecretsConfig {
    return new SealedSecretsConfig();
}
