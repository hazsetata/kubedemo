import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as sConfigLoader from "./sealed_secrets_config";

export interface SealedSecretsDeploymentArgs {
}

export class SealedSecretsDeployment extends pulumi.ComponentResource {
    constructor(name: string, args: SealedSecretsDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:SealedSecretsDeployment", name, {}, opts);
        let sConfig = sConfigLoader.getSealedSecretsConfig();

        const crdCG = new k8s.yaml.ConfigGroup("sealed-secrets-crd",
            {
                files: "kubernetes/sealed_secrets/yaml/sealedsecret-crd.yaml"
            },
            {
                parent: this
            }
        );

        let appDependsOn = [ crdCG ];

        if (sConfig.masterKeyPath) {
            const masterKeyCG = new k8s.yaml.ConfigGroup("sealed-secrets-key",
                {
                    files: sConfig.masterKeyPath
                },
                {
                    parent: this
                }
            );

            appDependsOn.push(masterKeyCG);
        }

        const deploymentCG = new k8s.yaml.ConfigGroup("sealed-secrets",
            {
                files: "kubernetes/sealed_secrets/yaml/controller.yaml"
            },
            {
                parent: this,
                dependsOn: appDependsOn
            }
        );
    }
}
