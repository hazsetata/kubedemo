import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as wfConfigLoader from "./weave_flux_config";
import {hasText} from "../../utils/textutils";

export interface WeaveFluxDeploymentArgs {
}

export class WeaveFluxDeployment extends pulumi.ComponentResource {
    constructor(name: string, args: WeaveFluxDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:k8s:WeaveFluxDeployment", name, {}, opts);
        const wfConfig = wfConfigLoader.getWeaveFluxConfig();

        const fluxNamespace = new k8s.core.v1.Namespace(`${name}-ns`,
            {
                metadata: {
                    name: "flux"
                }
            },
            {
                parent: this
            }
        );

        let appDependsOn = [];
        appDependsOn.push(fluxNamespace);

        let yamlFiles = [
            "kubernetes/weave_flux/yaml/flux-account.yaml",
            "kubernetes/weave_flux/yaml/flux-deployment.yaml",
            "kubernetes/weave_flux/yaml/memcache-dep.yaml",
            "kubernetes/weave_flux/yaml/memcache-svc.yaml"
        ];

        if (wfConfig.gitIdentityKeyPath) {
            const masterKeyCG = new k8s.yaml.ConfigGroup(`${name}-git-key`,
                {
                    files: wfConfig.gitIdentityKeyPath
                },
                {
                    parent: this,
                    dependsOn: [ fluxNamespace ]
                }
            );

            appDependsOn.push(masterKeyCG);
        }
        else {
            yamlFiles.push("kubernetes/weave_flux/yaml/flux-secret.yaml")
        }

        const wpApps = new k8s.yaml.ConfigGroup(`${name}-manifests`,
            {
                files: yamlFiles,
                transformations: [
                    (obj: any) => {
                        if (obj.kind == "Deployment" && obj.metadata.name == "flux") {
                            obj.spec.template.spec.containers[0].args.push(
                                `--git-url=${wfConfig.gitUrl}`,
                                `--git-branch=${wfConfig.gitBranch}`
                            );

                            if (hasText(wfConfig.gitPath)) {
                                obj.spec.template.spec.containers[0].args.push(
                                    `--git-path=${wfConfig.gitPath}`
                                );
                            }

                            if (wfConfig.gitReadOnlyAccess) {
                                obj.spec.template.spec.containers[0].args.push(
                                    "--git-readonly"
                                );
                            }
                        }
                    }
                ]
            },
            {
                parent: this,
                dependsOn: appDependsOn
            }
        );
    }
}
