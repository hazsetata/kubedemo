import * as pulumi from "@pulumi/pulumi";

export class WeaveFluxConfig {
    readonly gitUrl: string;
    readonly gitBranch: string;
    readonly gitPath?: string;
    readonly gitIdentityKeyPath?: string;
    readonly gitReadOnlyAccess: boolean;


    constructor() {
        const theConfig = new pulumi.Config("vttweaveflux");

        this.gitUrl = theConfig.require("gitUrl");
        this.gitBranch = theConfig.get("gitBranch")  || "master";
        this.gitPath = theConfig.get("gitPath");
        this.gitIdentityKeyPath = theConfig.get("gitIdentityKeyPath");
        this.gitReadOnlyAccess = theConfig.getBoolean("gitReadOnlyAccess") || false;
    }
}

export function getWeaveFluxConfig(): WeaveFluxConfig {
    return new WeaveFluxConfig();
}
