import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as docean from "@pulumi/digitalocean";
import * as coreConfig from "../core/config"
import * as doConfigLoader from "./do_kubernetes_config"
import {KubernetesCluster} from "../core/kubernetes_cluster";

export interface DigitalOceanKubernetesClusterDeploymentArgs {
}

export class DigitalOceanKubernetesCluster extends pulumi.ComponentResource implements KubernetesCluster {
    readonly kubernetesProvider: k8s.Provider;
    readonly clusterName: pulumi.Output<string>;
    readonly clusterFQDN: pulumi.Output<string>;
    readonly clusterCredentialsCommand: pulumi.Output<string>;

    constructor(name: string, args: DigitalOceanKubernetesClusterDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:core:DigitalOceanKubernetesCluster", name, {}, opts);
        const doConfig = doConfigLoader.getDigitalOceanKubernetesConfig();

        const doProvider = new docean.Provider("DO-Provider",
            {
                token: doConfig.apiToken
            },
            {
                parent: this
            }
        );

        const k8sCluster = new docean.KubernetesCluster(`${coreConfig.applicationName}-K8S`.toLowerCase(),
            {
                region: doConfig.region as docean.Region,
                version: doConfig.kubernetesVersion,
                nodePool: {
                    name: `${coreConfig.applicationName}-K8S-default`.toLowerCase(),
                    size: doConfig.nodeSize,
                    nodeCount:doConfig.nodeCount
                }
            },
            {
                provider: doProvider,
                ignoreChanges: [ "version" ],
                parent: this
            }
        );

        this.kubernetesProvider = new k8s.Provider(`${coreConfig.applicationName}-K8S-Provider`,
            {
                kubeconfig: k8sCluster.kubeConfigs[0].rawConfig,
            },
            {
                parent: this
            }
        );

        this.clusterName = k8sCluster.name;
        this.clusterFQDN = k8sCluster.endpoint;
        this.clusterCredentialsCommand = pulumi.interpolate `doctl kubernetes cluster kubeconfig save ${k8sCluster.name}`;

        this.registerOutputs({
            kubernetesProvider: this.kubernetesProvider,
            clusterName: this.clusterName,
            clusterFQDN: this.clusterFQDN,
            clusterCredentialsCommand: this.clusterCredentialsCommand
        });
    }
}