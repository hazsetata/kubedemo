import * as pulumi from "@pulumi/pulumi";
import * as docean from "@pulumi/digitalocean"

export class DigitalOceanKubernetesConfig {
    readonly region: string;
    readonly kubernetesVersion: string;
    readonly nodeCount: number;
    readonly nodeSize: string;
    readonly apiToken: string;

    constructor() {
        const doConfig = new pulumi.Config("vttdokubernetes");

        this.region = doConfig.get("region") || docean.Regions.AMS3;
        this.kubernetesVersion = doConfig.get("kubernetesVersion") || "latest";
        this.nodeCount = doConfig.getNumber("nodeCount") || 3;
        this.nodeSize = doConfig.get("nodeSize") || docean.DropletSlugs.DropletS2VCPU4GB;
        this.apiToken = doConfig.require("apiToken");
    }
}

export function getDigitalOceanKubernetesConfig(): DigitalOceanKubernetesConfig {
    return new DigitalOceanKubernetesConfig();
}
