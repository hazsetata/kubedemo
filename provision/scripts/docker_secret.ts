import {createDockerConfigJsonValue} from "../utils/textutils";

const chalk = require('chalk');
const clear = require('clear');
const inquirer   = require('inquirer');
const yaml = require('js-yaml');

clear();
console.log(chalk.green("Docker Image-Pull-Secret Generator"));

const questions = [
    {
        name: 'secretName',
        type: 'input',
        message: 'Enter the name of the secret to be created:',
        validate: function( value: string ) {
            if (value.length) {
                return true;
            } else {
                return 'Please enter the secret\'s name.';
            }
        }
    },
    {
        name: 'secretNamespace',
        type: 'input',
        message: 'Enter the namespace for the secret to be created:',
        default: "default",
        validate: function( value: string ) {
            if (value.length) {
                return true;
            } else {
                return 'Please enter the namespace\'s name.';
            }
        }
    },
    {
        name: 'registryName',
        type: 'input',
        message: 'Enter the name of the registry (without "https"):',
        validate: function( value: string ) {
            if (value.length) {
                return true;
            } else {
                return 'Please enter the registry\'s name.';
            }
        }
    },
    {
        name: 'userName',
        type: 'input',
        message: 'Enter your username at the registry:',
        validate: function( value: string ) {
            if (value.length) {
                return true;
            } else {
                return 'Please enter your username.';
            }
        }
    },
    {
        name: 'email',
        type: 'input',
        message: 'Enter your email address at the registry (optional):',
        validate: function( value: string ) {
            return true;
        }
    },
    {
        name: 'password',
        type: 'password',
        message: 'Enter your password:',
        validate: function( value: string ) {
            if (value.length) {
                return true;
            } else {
                return 'Please enter your password.';
            }
        }
    }
];

const run = async () => {
    let answers = await inquirer.prompt(questions);

    let secretContent = createDockerConfigJsonValue(
        answers.registryName,
        answers.userName,
        answers.password,
        answers.email
    );

    let secretObject = {
        "apiVersion": "v1",
        "kind": "Secret",
        "type": "kubernetes.io/dockerconfigjson",
        "metadata": {
            "name": answers.secretName,
            "namespace": answers.secretNamespace
        },
        "data": {
            ".dockerconfigjson": secretContent
        }
    };

    console.log(chalk.yellow(yaml.safeDump(secretObject, {"lineWidth": -1})));
};

run();
