import * as pulumi from "@pulumi/pulumi";
import * as cloudflare from "@pulumi/cloudflare";
import {DnsProviderResource} from "../core/dns_provider";
import * as cfConfigLoader from "./cloudflare_dns_config"

export interface CloudflareDnsProviderArgs {
    ips: pulumi.Input<string[]>,
}

export class CloudflareDnsProvider extends DnsProviderResource {
    constructor(name: string, args: CloudflareDnsProviderArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:cloudflare:DnsDeployment", name, {}, opts);
        const cfConfig = cfConfigLoader.getCloudflareDnsConfig();

        const cfProvider = new cloudflare.Provider("CF-Provider",
            {
                email: cfConfig.email,
                apiKey: cfConfig.apiKey
            },
            {
                parent: this
            }
        );

        let dnsName: string;
        if (cfConfig.dnsPostfix != null) {
            dnsName = `${pulumi.getStack()}.${cfConfig.dnsPostfix}`;
        }
        else {
            dnsName = pulumi.getStack();
        }

        let firstIp = pulumi.output(args.ips).apply(ips => `${ips[0]}`);

        const dnsEntry = new cloudflare.Record(`${name}-dns`,
            {
                name: dnsName,
                zoneId: cfConfig.zoneId,
                value: firstIp,
                type: "A",
                ttl: cfConfig.ttl
            },
            {
                provider: cfProvider,
                parent: this
            }
        );

        if (cfConfig.createWildcard) {
            let wildcardDnsName = `*.${dnsName}`;

            const wildcardDnsEntry = new cloudflare.Record(`${name}-dnswild`,
                {
                    name: wildcardDnsName,
                    zoneId: cfConfig.zoneId,
                    value: firstIp,
                    type: "A",
                    ttl: cfConfig.ttl
                },
                {
                    provider: cfProvider,
                    parent: this
                }
            );

            this.fullDomainName = wildcardDnsEntry.hostname;
        }
        else {
            this.fullDomainName = dnsEntry.hostname;
        }

        super.registerOutputs({
            fullDomainName: this.fullDomainName
        });
    }
}
