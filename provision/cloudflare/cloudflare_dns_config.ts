import * as pulumi from "@pulumi/pulumi";

export class CloudflareDnsConfig {
    readonly email: string;
    readonly apiKey: string;
    readonly zoneId: string;
    readonly dnsPostfix?: string;
    readonly createWildcard: boolean;
    readonly ttl: number;

    constructor() {
        const cloudflareConfig = new pulumi.Config("vttcloudflaredns");

        this.email = cloudflareConfig.require("email");
        this.apiKey = cloudflareConfig.require("apiKey");
        this.zoneId = cloudflareConfig.require("zoneId");

        this.dnsPostfix = cloudflareConfig.get("dnsPostfix");
        this.createWildcard = cloudflareConfig.getBoolean("createWildcard") || false;
        this.ttl = cloudflareConfig.getNumber("ttl") || 3600;
    }
}

export function getCloudflareDnsConfig(): CloudflareDnsConfig {
    return new CloudflareDnsConfig();
}
