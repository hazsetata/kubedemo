import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import {Base64} from "js-base64";
import {AcmeProviderResource} from "../core/acme_provider";
import * as cloudflareDnsConfigLoader from "./cloudflare_dns_config";

export interface CloudflareAcmeDnsProviderArgs {
    acmeEmail: pulumi.Input<string>;
    acmeStaging: pulumi.Input<boolean>;
    domainName: pulumi.Input<string>;
    certificateNamespace: pulumi.Input<string>;
}

export class CloudflareAcmeDnsProvider extends AcmeProviderResource {
    constructor(name: string, args: CloudflareAcmeDnsProviderArgs, opts?: pulumi.ComponentResourceOptions) {
        super("vtt:acme:CloudflareDnsProvider", name, {}, opts);
        const cloudflareConfig = cloudflareDnsConfigLoader.getCloudflareDnsConfig();

        // Modeled after this document:
        // https://docs.cert-manager.io/en/latest/tasks/acme/configuring-dns01/cloudflare.html

        let secretContent = Base64.encode(cloudflareConfig.apiKey);
        let issuerSecret = new k8s.core.v1.Secret("issuer-secret",
            {
                metadata: {
                    name: "cloudflare-dns-issuer-secret",
                    namespace: "cert-manager"
                },
                type: "Opaque",
                data: {
                    "api-key": secretContent
                }
            },
            {
                parent: this
            }
        );

        let stagingIssuer = new k8s.apiextensions.CustomResource("staging-issuer",
            {
                kind: "ClusterIssuer",
                apiVersion: "cert-manager.io/v1alpha2",
                metadata: {
                    name: "letsencrypt-cloudflare-dns-staging",
                    namespace: "cert-manager"
                },
                spec: {
                    acme: {
                        server: "https://acme-staging-v02.api.letsencrypt.org/directory",
                        email: args.acmeEmail,
                        privateKeySecretRef: {
                            name: "letsencrypt-cloudflare-dns-staging"
                        },
                        solvers: [
                            {
                                selector: {},
                                dns01: {
                                    cloudflare: {
                                        email: cloudflareConfig.email,
                                        apiKeySecretRef: {
                                            name: "cloudflare-dns-issuer-secret",
                                            key: "api-key"
                                        }
                                    }
                                }
                            }
                        ]
                    }
                }
            },
            {
                dependsOn: [ issuerSecret ],
                parent: this
            }
        );

        let productionIssuer = new k8s.apiextensions.CustomResource("production-issuer",
            {
                kind: "ClusterIssuer",
                apiVersion: "cert-manager.io/v1alpha2",
                metadata: {
                    name: "letsencrypt-cloudflare-dns-production",
                    namespace: "cert-manager"
                },
                spec: {
                    acme: {
                        server: "https://acme-v02.api.letsencrypt.org/directory",
                        email: args.acmeEmail,
                        privateKeySecretRef: {
                            name: "letsencrypt-cloudflare-dns-production"
                        },
                        solvers: [
                            {
                                selector: {},
                                dns01: {
                                    cloudflare: {
                                        email: cloudflareConfig.email,
                                        apiKeySecretRef: {
                                            name: "cloudflare-dns-issuer-secret",
                                            key: "api-key"
                                        }
                                    }
                                }
                            }
                        ]
                    }
                }
            },
            {
                dependsOn: [ issuerSecret ],
                parent: this
            }
        );

        let ingressCertificate: k8s.apiextensions.CustomResource;
        let isStaging = pulumi.output(args.acmeStaging);

        isStaging.apply(staging => {
            ingressCertificate = new k8s.apiextensions.CustomResource("ingress-cert",
                {
                    kind: "Certificate",
                    apiVersion: "cert-manager.io/v1alpha2",
                    metadata: {
                        name: "ingress-certificate",
                        namespace: args.certificateNamespace
                    },
                    spec: {
                        secretName: "ingress-secret",
                        issuerRef: {
                            name: staging ? "letsencrypt-cloudflare-dns-staging" : "letsencrypt-cloudflare-dns-production",
                            kind: "ClusterIssuer"
                        },
                        dnsNames: [
                            args.domainName
                        ]
                    }
                },
                {
                    dependsOn: [ stagingIssuer, productionIssuer ],
                    parent: this
                }
            );
        });

        this.stagingIssuerName = pulumi.output("letsencrypt-cloudflare-dns-staging");
        this.productionIssuerName = pulumi.output("letsencrypt-cloudflare-dns-production");
    }
}
